#!/usr/bin/env bash
# @file   setup_lg_test_challenge.sh
#
# @author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>
#
# @date   10 May 2023
#
# @brief  sets up a test fof challenge
#
# Copyright © 2023 Dj Djundjila, TTS Rebuild Committee
#
# DjudgePortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DjudgePortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -Eeuo pipefail

export PHOTOC_NAME=test_photocontest
export CLI="flask --app DjudgePortal"

${CLI} add-photocontest-challenge djundjila ${PHOTOC_NAME} ${PHOTOC_NAME}_title https://www.reddit.com/r/Wetshaving/comments/ucik9d/announcing_the_eighth_annual_photocontest_games/ "Test instance using lg 22 posts for photocontest"

${CLI} add-djudge ${PHOTOC_NAME} djundjila enndeegee Semaj3000

${CLI} activate-challenge ${PHOTOC_NAME}

${CLI} add-challenge-group photocontest
${CLI} add-challenge-group photocontest ${PHOTOC_NAME}

export URL=https://reddit.com/r/Wetshaving/comments/v2a1la/wednesday_lather_games_sotd_thread_jun_01_2022/
${CLI} add-post-and-campaign ${URL} photocontest 1 "Invitation" "Score for photocontest"

export URL=https://reddit.com/r/Wetshaving/comments/v32374/thursday_lather_games_sotd_thread_jun_02_2022/
${CLI} add-post-and-campaign ${URL} photocontest 2 "Invitation" "Score for photocontest"

export URL=https://reddit.com/r/Wetshaving/comments/v3sr4i/friday_lather_games_sotd_thread_jun_03_2022/
${CLI} add-post-and-campaign ${URL} photocontest 3 "Invitation" "Score for photocontest"

