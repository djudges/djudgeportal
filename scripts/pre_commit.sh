#!/usr/bin/env bash
# @file   pre_commit.sh
#
# @author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>
#
# @date   16 Apr 2023
#
# @brief  for changed files only: lint and check for common errors
#
# Copyright © 2023 Dj Djundjila, TTS Rebuild Committee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


changed_files=$(git diff --diff-filter=d --cached --name-only)

if [ ! -z "$changed_files" ]
then
    echo "changed files:"
    echo "$changed_files"
fi

echo "changed_files =  $changed_files"
py_files="$(echo "$changed_files" |grep -E '\.(py)$')"

set -eu

if [ ! -z "$py_files" ]
then
    echo "changed python files:"
    echo "$py_files"
fi

if [ -z "$py_files" ]
then
    echo "No changes to any python files, linting skipped."
else
    black --required-version 23 --check $py_files

    pyflakes $py_files
fi

