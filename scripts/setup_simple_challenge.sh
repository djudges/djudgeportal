#!/usr/bin/env bash
# @file   setup_simple_challenge.sh
#
# @author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>
#
# @date   13 Apr 2023
#
# @brief  sets up a simple challenge and users
#
# Copyright © 2023 Dj Djundjila, TTS Rebuild Committee
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -Eeuo pipefail

flask --app DjudgePortal clear-db
flask --app DjudgePortal init-db
flask --app DjudgePortal add-admin admin
flask --app DjudgePortal get-access-token admin
flask --app DjudgePortal add-challenge owner test_challenge test_challenge_title www.example.challenge.url SEARCH_STRING "challenge description"
flask --app DjudgePortal get-access-token owner
flask --app DjudgePortal run --debug

