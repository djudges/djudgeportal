#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   auth.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   09 Jul 2022

@brief  handles login using reddit messages for authentication

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from flask import (
    Blueprint,
    flash,
    redirect,
    render_template,
    request,
    session,
    url_for,
    current_app,
)

from DjudgePortal import orms
from DjudgePortal.db import get_db

from datetime import datetime

import logging

from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, BooleanField
from wtforms.validators import InputRequired, ValidationError

from flask_login import login_user, login_required, logout_user

bp = Blueprint("auth", __name__, url_prefix="/auth")


@bp.route("/login", methods=("GET", "POST"))
def login():
    db_session = get_db()
    with db_session.begin():
        user_names = {t[0] for t in db_session.query(orms.User.user_name_lower)}

        def validate_token(form, field):
            user = (
                db_session.query(orms.User)
                .filter_by(user_name=form.user_name.data)
                .one_or_none()
            )
            if user is None:
                raise ValidationError(f"User '{form.user_name.data}' does not exist.")
            elif user.access_code is None:
                raise ValidationError(
                    "You haven't requested an access code for user "
                    f"'u/{user.user_name}' yet. Please follow the link to do "
                    "so."
                )
            elif user.access_code_valid_until < datetime.now().timestamp():
                raise ValidationError(
                    f"User u/{user.user_name}'s access code has expired, "
                    "please request a new one."
                )
            elif field.data != user.access_code:
                raise ValidationError("Incorrect access code.")

        def validate_user_name(form, field):
            if field.data.lower() not in user_names:
                raise ValidationError(
                    f"It seems user u/{field.data} is not an organiser of, "
                    "nor djudge or participant in any of the challenges I run"
                )

        class LoginForm(FlaskForm):
            user_name = StringField(
                "Reddit user name", validators=[InputRequired(), validate_user_name]
            )
            access_token = IntegerField(
                "Access token", validators=[InputRequired(), validate_token]
            )
            remember_me = BooleanField("Remember me", default=True)
            submit = SubmitField("Submit")
            cancel = SubmitField("Cancel")

        form = LoginForm()
        if form.errors:
            flash(form.errors)
            print(form.errors)
        if request.method == "POST":
            print("got POST request")
            print("form validated?", form.validate())
            print(form.errors)
            print(form)
        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(url_for("challenge.index"))
            user = (
                db_session.query(orms.User)
                .filter_by(user_name=form.user_name.data)
                .one_or_none()
            )
            login_user(user, remember=form.remember_me.data)
            flash("Logged in successfully.")
            return redirect(url_for("challenge.index"))

    to_name = current_app.config["REDDIT_IDENTITY_BOT"]
    return render_template(
        "auth/login.html",
        form=form,
        to_name=to_name,
        message_url=(
            f"https://www.reddit.com/message/compose/?to={to_name}&subject="
            "Access%20Code&message=Pretty%20please!"
        ),
    )


@bp.route("/login_legacy", methods=("GET", "POST"))
def login_legacy():
    if request.method == "POST":
        logging.debug("logging in")
        user_name = request.form["user_name"].strip()
        access_code = request.form["access_code"]
        logging.info(
            f"log in attempt for user_name '{user_name}' with "
            f"access_code '{access_code}'"
        )
        db, orms = get_db()
        error = None

        def int_or_none(rep_string):
            try:
                return int(rep_string)
            except ValueError:
                return "FALSE"

        logging.debug("trying to start transaction to query user")
        with db.session.begin():
            logging.debug("started transaction")
            user = (
                db.session.query(orms.User)
                .filter_by(user_name_lower=user_name.lower())
                .one_or_none()
            )
            if user is None:
                error = f"User '{user_name}' is not registered."
            elif user.access_code is None:
                error = (
                    "You haven't requested an access code for user "
                    f"'{user_name}' yet. Please follow the link to do "
                    "so."
                )
            elif user.access_code_valid_until < datetime.now().timestamp():
                error = (
                    f"User {user_name}'s access code has expired, "
                    "please request a new one."
                )
            elif int_or_none(access_code) != user.access_code:
                print(
                    f"access_code ({access_code}) != user.access_code "
                    f"({user.access_code})"
                )
                error = "Incorrect access code."

            if error is None:
                logging.info(f"successfully logged in user '{user_name}'")
                session.clear()
                session["user_pk"] = user.pk
                return redirect(url_for("challenge.index"))
            logging.debug("ended transaction")
        logging.error(f"failed login with error '{error}'")
        flash(error)

    to_name = current_app.config["REDDIT_IDENTITY_BOT"]
    return render_template(
        "auth/login_legacy.html",
        message_url=(
            f"https://www.reddit.com/message/compose/?to={to_name}&subject="
            "Access%20Code&message=Pretty%20please!"
        ),
    )


# @bp.before_app_request
# def load_logged_in_user():
#     user_pk = session.get('user_pk')

#     if user_pk is None:
#         g.user = None
#     else:
#         db, orms = get_db()
#         g.user_pk = user_pk
#         logging.debug("trying to start transaction in load_logged_in_user")
#         with db.session.begin():
#             logging.debug("started transaction")
#             g.user = db.session.query(orms.User).filter_by(
#                 pk=user_pk).one_or_none()
#             logging.debug("ended transaction")


# def login_required(view):
#     @functools.wraps(view)
#     def wrapped_view(**kwargs):
#         if g.user is None:
#             return redirect(url_for('auth.login'))

#         return view(**kwargs)

#     return wrapped_view


@bp.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("challenge.index"))
