#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   djudgements.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   17 Aug 2022

@brief  handling of djudgements

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import (
    Blueprint,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from werkzeug.exceptions import abort
from flask_wtf import FlaskForm
from wtforms import (
    BooleanField,
    DecimalRangeField,
    TextAreaField,
    RadioField,
    SubmitField,
)
from wtforms.validators import InputRequired
from flask_login import current_user


from ..auth import login_required
from ..db import get_db
from .. import orms


bp = Blueprint("djudgement", __name__)


@bp.route("/djudgement/index/<int:djudge_pk>")
@login_required
def index(djudge_pk):
    db_session = get_db()
    with db_session.begin():
        if not current_user.is_admin and current_user.pk != djudge_pk:
            abort(403, "You are not authorised to see this.")
        djudge = db_session.get(orms.User, djudge_pk)
        if djudge is None:
            abort(404, "User does not exist")

        return render_template("djudgement/index.html", djudge=djudge)


def get_NumericField(label):
    return DecimalRangeField(label, validators=[InputRequired()])


def get_BooleanField(label):
    return BooleanField(label=label, default=False)


def get_multiple_choice_field(category):
    return RadioField(
        choices=sorted(((c.pk, c.label) for c in category.choices)),
        coerce=int,
    )


def get_TextField(label):
    return TextAreaField(label=label)


@bp.route("/djudgement/adjudication_redirection/<int:sotd_association_pk>")
def adjudication_redirection(sotd_association_pk):
    db_session = get_db()
    with db_session.begin():
        sotd_association = db_session.get(orms.SOTDAssociation, sotd_association_pk)
        if sotd_association is None:
            abort(404, "SOTD not associated")
        invitation = (
            db_session.query(orms.DjudgementInvitation)
            .filter_by(sotd_association=sotd_association)
            .one_or_none()
        )
        if invitation is None:
            abort(404, "Invitation does not exist")
        return redirect(url_for("djudgement.adjudicate", invitation_pk=invitation.pk))


@bp.route("/djudgement/adjudicate/<int:invitation_pk>", methods=("GET", "POST"))
@login_required
def adjudicate(invitation_pk):
    db_session = get_db()
    with db_session.begin():
        invitation = db_session.get(orms.DjudgementInvitation, invitation_pk)
        if invitation is None:
            abort(404, "Invitation does not exist")
        sotd_association = invitation.sotd_association
        if (
            not sotd_association.meta_data_checked
            and invitation.challenge.has_meta_data_check
        ):
            return redirect(
                url_for("meta_data.validate", sotd_association_pk=sotd_association.pk)
            )
        if invitation.state == invitation.States.Djudged:
            return redirect(
                url_for("djudgement.edit", djudgement_pk=invitation.djudgement.pk)
            )
        if not current_user.pk == invitation.sotd_association.djudge_pk:
            abort(403, "You are not authorised to see this.")

        items = dict(
            submit=SubmitField("Submit Djudgement"),
            cancel=SubmitField("Cancel Djudgement"),
        )

        for category in invitation.categories:
            if category.category_type == "NumericCategory":
                field = get_NumericField(category.title)
            elif category.category_type in (
                "TextCategory",
                "DisqualificationCategory",
                "NoteCategory",
            ):
                field = get_TextField(category.title)
            elif category.category_type in (
                "BooleanCategory",
                "ConditionalValueCategory",
            ):
                field = get_BooleanField(category.title)
            elif category.category_type == "MultipleChoiceCategory":
                field = get_multiple_choice_field(category)
            else:
                raise ValueError(f"category '{category}' of unknown type")
            items[category.title] = field

        DjudgementForm = type("DjudgementForm", (FlaskForm,), items)
        form = DjudgementForm()
        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(
                    url_for("djudgement.adjudicate", invitation_pk=invitation_pk)
                )
            djudgement = orms.Djudgement(djudgement_invitation=invitation)
            db_session.add(djudgement)
            for category in invitation.categories:
                djudgement.scores.append(
                    category.new_score(
                        value=getattr(form, category.title).data,
                        djudgement=djudgement,
                    )
                )
            db_session.add(djudgement)
            invitation.state = invitation.States.Djudged
            db_session.flush()
            invitation.challenge.update_tally_for_user(
                invitation.sotd.author, db_session
            )

            if current_user.open_djudgement_invitations:
                return redirect(
                    url_for(
                        "djudgement.adjudicate",
                        invitation_pk=current_user.open_djudgement_invitations[0].pk,
                    )
                )
            elif current_user.open_selections:
                return redirect(
                    url_for(
                        "djudgement.select",
                        selection_pk=current_user.open_selections[0].pk,
                    )
                )
            return redirect(url_for("djudgement.index", djudge_pk=invitation.djudge_pk))
        if request.method == "POST":
            flash(form.errors, "error")

        return render_template(
            "djudgement/adjudicate.html",
            form=form,
            invitation=invitation,
            current_user=current_user,
            categories=sorted(invitation.categories, key=lambda x: x.pk),
        )


@bp.route("/djudgement/edit/<int:djudgement_pk>", methods=("GET", "POST"))
@login_required
def edit(djudgement_pk):
    db_session = get_db()
    with db_session.begin():
        djudgement = db_session.get(orms.Djudgement, djudgement_pk)
        if djudgement is None:
            abort(404, "Djudgement does not exist")
        invitation = djudgement.djudgement_invitation
        if not current_user.pk == invitation.sotd_association.djudge_pk:
            abort(403, "You are not authorised to see this.")

        class DjudgementForm(FlaskForm):
            submit = SubmitField("Submit Djudgement")
            cancel = SubmitField("Cancel Djudgement")

        for category in invitation.categories:
            if category.category_type == "NumericCategory":
                field = DecimalRangeField(category.title)
            elif category.category_type in (
                "TextCategory",
                "DisqualificationCategory",
                "NoteCategory",
            ):
                field = get_TextField(category.title)
            elif category.category_type in (
                "BooleanCategory",
                "ConditionalValueCategory",
            ):
                field = get_BooleanField(category.title)
            elif category.category_type == "MultipleChoiceCategory":
                field = get_multiple_choice_field(category)
            else:
                raise ValueError(f"category '{category}' of unknown type")
            setattr(DjudgementForm, category.title, field)
        form = DjudgementForm(
            data={score.category.title: score.value for score in djudgement.scores}
        )
        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(url_for("user.view", pk=invitation.djudge_pk))
            for score in djudgement.scores:
                score.value = getattr(form, score.category.title).data
            db_session.flush()
            invitation.challenge.update_tally_for_user(
                invitation.sotd.author, db_session
            )
            if request.referrer:
                return redirect(url_for("user.view", pk=invitation.djudge_pk))

        if request.method == "POST":
            flash(form.errors, "error")

        return render_template(
            "djudgement/adjudicate.html",
            form=form,
            invitation=invitation,
            current_user=current_user,
            categories=sorted(invitation.categories, key=lambda x: x.pk),
        )


@bp.route("/selection/select/<int:selection_pk>", methods=("GET", "POST"))
@login_required
def select(selection_pk):
    db_session = get_db()
    with db_session.begin():
        selection = db_session.get(orms.Selection, selection_pk)
        if selection is None:
            abort(404, "Selection does not exist")
        if not current_user.pk == selection.djudge_pk:
            abort(403, "You are not authorised to see this.")

        choices = {
            f"Option {i}": sotd for (i, sotd) in enumerate(selection.sotds, start=1)
        }

        class SelectionForm(FlaskForm):
            submit = SubmitField("Submit Selection")
            cancel = SubmitField("Cancel Selection")
            choice = RadioField(
                "Options",
                choices=[(val.pk, key) for (key, val) in choices.items()],
                default=selection.selected_sotd_pk,
                coerce=int,
            )

        form = SelectionForm()

        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(url_for("user.view", pk=selection.djudge_pk))

            selection.selected_sotd_pk = form.choice.data
            db_session.flush()
            for user in {sotd.author for sotd in selection.sotds}:
                selection.selection_template.challenge.update_tally_for_user(
                    user, db_session
                )

            if current_user.open_selections:
                return redirect(
                    url_for(
                        "djudgement.select",
                        selection_pk=current_user.open_selections[0].pk,
                    )
                )
            elif current_user.open_djudgement_invitations:
                return redirect(
                    url_for(
                        "djudgement.adjudicate",
                        invitation_pk=current_user.open_djudgement_invitations[0].pk,
                    )
                )
            if request.referrer:
                return redirect(url_for("user.view", pk=selection.djudge_pk))

            return redirect(url_for("djudgement.index", djudge_pk=selection.djudge_pk))
        if request.method == "POST":
            flash(form.errors, "error")

        return render_template(
            "djudgement/select.html",
            form=form,
            selection=selection,
            current_user=current_user,
            choices=choices,
        )


@bp.route("/selection/view/<int:template_pk>")
def selection_view(template_pk):
    db_session = get_db()
    with db_session.begin():
        selection_template = db_session.get(orms.SelectionTemplate, template_pk)
        if selection_template is None:
            abort(404, "Selection template not found")
        return render_template(
            "djudgement/selection_view.html", template=selection_template
        )
