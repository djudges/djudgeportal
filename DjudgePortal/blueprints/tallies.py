#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   tallies.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   17 May 2023

@brief  Visualising tallies

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import (
    Blueprint,
    render_template,
    Response,
    request,
    flash,
    url_for,
    redirect,
)
from werkzeug.exceptions import abort
from flask_login import current_user

from ..auth import login_required
from ..db import get_db
from .. import orms
from sqlalchemy import select

import pandas as pd
import csv
from io import StringIO


bp = Blueprint("tallies", __name__)


def compute_tallies(challenge_pk, sort_column):
    db_session = get_db()
    with db_session.begin():
        challenge = db_session.get(orms.ChallengeBase, challenge_pk)
        if challenge is None:
            abort(404, "Challenge does not exist")
        user = db_session.get(orms.User, current_user.pk)
        if (
            user not in set(challenge.djudges).union(challenge.owners)
            and not current_user.is_admin
        ):
            abort(403, "You are not authorised to see this")
        sotd_tallies = (
            select(orms.User.pk, orms.Category.title, orms.SOTDPointsTally.value)
            .select_from(orms.User)
            .join(orms.SOTDPointsTally, orms.SOTDPointsTally.user_pk == orms.User.pk)
            .join(orms.Category, orms.Category.pk == orms.SOTDPointsTally.category_pk)
            .where(orms.SOTDPointsTally.challenge_pk == challenge_pk)
        )

        selection_tallies = (
            select(
                orms.User.pk, orms.SelectionTemplate.title, orms.SelectionTally.value
            )
            .select_from(orms.User)
            .join(orms.SelectionTally, orms.SelectionTally.user_pk == orms.User.pk)
            .join(
                orms.SelectionTemplate,
                orms.SelectionTemplate.pk == orms.SelectionTally.selection_template_pk,
            )
            .where(orms.SelectionTemplate.challenge_pk == challenge_pk)
        )

        scavenger_hunt_tallies = (
            select(
                orms.User.pk, orms.ScavengerHunt.title, orms.ScavengerHuntTally.value
            )
            .select_from(orms.User)
            .join(
                orms.ScavengerHuntTally, orms.ScavengerHuntTally.user_pk == orms.User.pk
            )
            .join(
                orms.ScavengerHunt,
                orms.ScavengerHunt.pk == orms.ScavengerHuntTally.scavenger_hunt_pk,
            )
            .where(orms.ScavengerHunt.challenge_pk == challenge_pk)
        )

        sponsor_tallies = (
            select(
                orms.User.pk, orms.SponsorPoints.title, orms.SponsorPointsTally.value
            )
            .select_from(orms.User)
            .join(
                orms.SponsorPointsTally, orms.SponsorPointsTally.user_pk == orms.User.pk
            )
            .join(
                orms.SponsorPoints,
                orms.SponsorPoints.pk == orms.SponsorPointsTally.sponsor_points_pk,
            )
            .where(orms.SponsorPoints.challenge_pk == challenge_pk)
        )

        bonus_tallies = (
            select(orms.User.pk, orms.UsageBonus.title, orms.UsageBonusTally.value)
            .select_from(orms.User)
            .join(orms.UsageBonusTally, orms.UsageBonusTally.user_pk == orms.User.pk)
            .join(
                orms.UsageBonus,
                orms.UsageBonus.pk == orms.UsageBonusTally.usage_bonus_pk,
            )
            .where(orms.UsageBonus.challenge_pk == challenge_pk)
        )

        stmt = sotd_tallies.union(selection_tallies)
        stmt = scavenger_hunt_tallies.union(stmt)
        stmt = sponsor_tallies.union(stmt)
        stmt = bonus_tallies.union(stmt)
        df = pd.read_sql(stmt, db_session.bind)

        def aggfunc(x):
            if len(x) != 1:
                raise ValueError(
                    "I caught a collection to aggregate but expected a single value"
                )
            return sum(x)

        crosstab = pd.crosstab(
            df.pk,
            df.title,
            dropna=False,
            values=df.value,
            aggfunc=aggfunc,
        )
        users = [db_session.get(orms.User, int(v)) for v in crosstab.index.values]

        headings = ["#", "User", "Total"] + list(crosstab.columns.values)
        if sort_column is None:
            sort_column_id = 0
        else:
            try:
                sort_column_id = headings.index(sort_column) - 2
            except ValueError as err:
                message = f"Column '{sort_column}' does not exist."
                flash(message)
                raise err
        print(f"sort_colum = '{sort_column}', sort_column_id = {sort_column_id}")
        rows = [
            (i, *r)
            for (i, r) in enumerate(
                sorted(
                    (
                        (user, [sum(s)] + list(s))
                        for (user, (_, s)) in zip(users, crosstab.iterrows())
                    ),
                    key=lambda x: -x[-1][sort_column_id],
                ),
                start=1,
            )
        ]

        return challenge, headings, rows


@bp.route("/tallies/<int:challenge_pk>")
@login_required
def tallies(challenge_pk):
    sort_column = request.args.get("sort_column")
    try:
        challenge, headings, rows = compute_tallies(challenge_pk, sort_column)
    except ValueError:
        return redirect(url_for("tallies.tallies", challenge_pk=challenge_pk))
    return render_template(
        "tallies/tallies.html",
        challenge=challenge,
        headings=headings,
        rows=rows,
        sort_column=sort_column,
    )


@bp.route("/tallies/<int:challenge_pk>.csv")
@login_required
def tallies_csv(challenge_pk):
    sort_column = request.args.get("sort_column")
    print("SORT COLUMN = ", sort_column)
    try:
        _, headings, rows = compute_tallies(challenge_pk, sort_column)
    except ValueError:
        return redirect(url_for("tallies.tallies", challenge_pk=challenge_pk))
    csv_list = list()
    csv_list.append(headings)
    csv_list += [(r[0], r[1].user_name) + tuple(r[2]) for r in rows]
    si = StringIO()
    cw = csv.writer(si)
    cw.writerows(csv_list)
    return Response(
        si.getvalue(),
        mimetype="text/csv",
        headers={"Content-disposition": "attachment; filename=users.csv"},
    )
