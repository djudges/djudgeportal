#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   challenges.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   18 Jul 2022

@brief  handling of different challenges

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from flask import (
    Blueprint,
    redirect,
    render_template,
    url_for,
    current_app,
    request,
    flash,
)
from werkzeug.exceptions import abort
from collections import defaultdict

from sqlalchemy import func, select, text

from ..db import get_db
from ..auth import login_required
from .. import orms

from flask_login import current_user

import praw
import prawcore

from flask_wtf import FlaskForm
from wtforms import (
    StringField,
    FormField,
    FieldList,
    BooleanField,
    Form,
    TextAreaField,
    SubmitField,
    SelectField,
    URLField,
)
from wtforms.validators import InputRequired, ValidationError, Optional, URL


bp = Blueprint("challenge", __name__)


@bp.route("/favicon.ico")
def favicon():
    return redirect(url_for("static", filename="favicon.ico"))


@bp.route("/index")
@bp.route("/")
def index():
    db_session = get_db()
    with db_session.begin():
        challenges = db_session.query(orms.ChallengeBase).all()

        return render_template("challenge/index.html", my_challenges=challenges)


def get_challenge(challenge_pk, db_session, orms):
    challenge = db_session.get(orms.ChallengeBase, challenge_pk)
    if challenge is None:
        abort(404, f"Challenge id {challenge_pk} does not exist")
    return challenge


@bp.route("/<int:id>/view")
def view(id):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(id, db_session, orms)
        undjudged = (
            db_session.query(orms.DjudgementInvitation)
            .filter_by(state=orms.DjudgementInvitation.States.Unacknowledged)
            .join(orms.SOTDAssociation)
            .filter_by(challenge_pk=challenge.pk)
            .order_by(orms.DjudgementInvitation.issued)
        )
        undjudged_dict = defaultdict(list)
        for invite in undjudged:
            undjudged_dict[invite.djudge].append(invite)
        return render_template(
            "challenge/view.html", challenge=challenge, undjudged=undjudged_dict
        )


class CategoryForm(Form):
    title = StringField("Title")
    guidance = TextAreaField("Guidance")


class CategoryEditForm(CategoryForm):
    category_type = SelectField(
        "Category Type",
        choices=[("numeric", "Numeric"), ("boolean", "Yes/no"), ("text", "Text")],
        render_kw={"readonly": True},
    )
    delete = SubmitField("Delete")


class CategoryCreateForm(CategoryForm):
    category_type = SelectField(
        "Category Type",
        choices=[("numeric", "Numeric"), ("boolean", "Yes/no"), ("text", "Text")],
    )
    submit = SubmitField("Add")
    cancel = SubmitField("Cancel")


def verify_reddit_user(form, field):
    print(f"I got field.data = {field.data}")
    return get_user(field.data)


def get_user(user_name):
    db_session = get_db()
    user = (
        db_session.query(orms.User)
        .filter_by(user_name_lower=user_name.lower())
        .one_or_none()
    )
    if user:
        return user

    reddit = praw.Reddit(current_app.config["REDDIT_CONFIG"], user_agent="DjudgePortal")
    print(f"user_name = '{user_name}'")
    praw_user = reddit.redditor(user_name)
    try:  # to make sure the user name exists
        _ = praw_user.id
    except prawcore.exceptions.NotFound as e:
        raise ValidationError(e)
    user = orms.User(user_name=praw_user.name)
    return user


class UserCreateForm(Form):
    user_name = StringField("User name", validators=[Optional(), verify_reddit_user])
    submit = SubmitField("Add")
    cancel = SubmitField("Cancel")


class UserShowForm(Form):
    user_name = StringField(
        "User name", validators=[InputRequired()], render_kw={"readonly": True}
    )
    delete = SubmitField("Delete")


def validate_dates(form, end_date_field):
    if end_date_field.data <= form.start_date.data:
        raise ValidationError(
            (
                f"The end date {end_date_field.data} needs to be later than the "
                f"start date {form.start_date.data}."
            )
        )
    return


class ChallengeForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired("A name is required")])
    title = StringField("Title", validators=[InputRequired("A name is required")])
    post_url = URLField("Post URL", validators=[URL()])
    search_string = StringField("Search string", validators=[InputRequired()])
    description = TextAreaField(
        "Description", validators=[InputRequired()], render_kw={"width": "90%"}
    )
    categories = FieldList(FormField(CategoryEditForm))
    add_category = FormField(CategoryCreateForm)
    owners = FieldList(FormField(UserShowForm))
    add_owner = FormField(UserCreateForm)
    djudges = FieldList(FormField(UserShowForm))
    add_djudge = FormField(UserCreateForm)
    is_active = BooleanField(
        (
            "Challenge is active (active challenges cannot be modified after "
            "participants have joined)"
        )
    )
    update = SubmitField("Update")
    cancel = SubmitField("Cancel")


class ChallengeGroupForm(FlaskForm):
    next_post_url = StringField("URL for next post")
    alert_djudges = BooleanField(
        "Djudges of this challenge group can receive reminder messages"
    )
    clear_url = SubmitField("Clear URL")
    update = SubmitField("Update")
    cancel = SubmitField("Cancel")


@bp.route("/group_view/<int:group_pk>", methods=("GET", "POST"))
@login_required
def group_view(group_pk):
    db_session = get_db()
    with db_session.begin():
        if not current_user.is_admin:
            abort(403, "You're not an admin")
        group = db_session.get(orms.ChallengeGroup, group_pk)
        if group is None:
            abort(404, "unknown group")

        form = ChallengeGroupForm(data=group)
        if form.validate_on_submit():
            if form.cancel.data:
                return redirect(url_for("user.view", pk=current_user.pk))

            if form.clear_url.data:
                group.next_post_url = None
                return redirect(url_for("user.view", pk=current_user.pk))
            group.next_post_url = form.next_post_url.data
            group.alert_djudges = form.alert_djudges.data
            return redirect(url_for("user.view", pk=current_user.pk))
        return render_template(
            "challenge/group_view.html",
            form=form,
            current_user=current_user,
            group=group,
        )


@bp.route("/djudge_ranking/<int:challenge_pk>")
@login_required
def djudge_ranking(challenge_pk):
    sort_column = request.args.get("sort_column")
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_pk, db_session, orms)
        if not current_user.is_admin and current_user not in (
            challenge.owners + challenge.djudges
        ):
            abort(403, "You're not an admin, djudge, or owner")
        djudges = challenge.djudges
        nb_djudgements, nb_dqs = challenge.count_djudgements()
        djudge_points = challenge.compute_djudge_points()

        def row(djudge):
            n_tot = nb_djudgements[djudge]
            n_dqs = nb_dqs[djudge]
            points = djudge_points[djudge]
            return (djudge, n_dqs / n_tot * 100, points / n_tot)

        headings = ("#", "Djudge", "DQ rate", "per SOTD")
        if sort_column is None:
            sort_column_id = 1
        else:
            try:
                sort_column_id = headings[2:].index(sort_column) + 1
            except ValueError as err:
                message = f"Column '{sort_column}' does not exist."
                flash(message)
                raise err

        rows = [
            (i, *row)
            for (i, row) in enumerate(
                sorted(
                    (row(djudge) for djudge in djudges),
                    key=lambda row: -row[sort_column_id],
                ),
                start=1,
            )
        ]
        return render_template(
            "challenge/djudge_ranking.html",
            challenge=challenge,
            rows=rows,
            headings=headings,
            sort_column_id=sort_column_id,
        )


@bp.route("/brands_stats/<int:challenge_pk>")
@login_required
def brands_stats(challenge_pk):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_pk, db_session, orms)
        if not current_user.is_admin and current_user not in (
            challenge.owners + challenge.djudges
        ):
            abort(403, "You're not an admin, djudge, or owner")

        def sel(orm_type):
            return db_session.execute(
                select(
                    orm_type.brand,
                    func.count(orm_type.brand).label("count"),
                )
                .join(
                    orms.SOTDMetaDataAssociation,
                    orms.SOTDMetaDataAssociation.metadata_item_pk == orm_type.pk,
                )
                .join(
                    orms.SOTDAssociation,
                    orms.SOTDAssociation.pk
                    == orms.SOTDMetaDataAssociation.sotd_association_pk,
                )
                .where(orms.SOTDAssociation.challenge == challenge)
                .group_by(orm_type.brand)
                .order_by(text("count DESC"))
            )

        lather_brands = sel(orms.WSDBLather)
        postshave_brands = sel(orms.WSDBPostshave)
        fragrance_brands = sel(orms.WSDBFragrance)
        brand_usage = {
            "Lathers": lather_brands,
            "Post Shaves": postshave_brands,
            "Fragrances": fragrance_brands,
        }

        return render_template(
            "challenge/brand_stats.html", challenge=challenge, brand_usage=brand_usage
        )


@bp.route("/brands_stats_by_day/<int:challenge_pk>")
@login_required
def brands_stats_by_day(challenge_pk):
    db_session = get_db()
    with db_session.begin():
        challenge = get_challenge(challenge_pk, db_session, orms)
        if not current_user.is_admin and current_user not in (
            challenge.owners + challenge.djudges
        ):
            abort(403, "You're not an admin, djudge, or owner")

        def sel(orm_type):
            return db_session.execute(
                select(
                    orm_type.brand,
                    orms.Post,
                    orms.PostAssociation.day.label("day"),
                    func.count(orm_type.brand).label("count"),
                )
                .join(
                    orms.SOTDMetaDataAssociation,
                    orms.SOTDMetaDataAssociation.metadata_item_pk == orm_type.pk,
                )
                .join(
                    orms.SOTDAssociation,
                    orms.SOTDAssociation.pk
                    == orms.SOTDMetaDataAssociation.sotd_association_pk,
                )
                .join(
                    orms.PostAssociation,
                    orms.PostAssociation.challenge_pk == challenge_pk,
                )
                .join(orms.SOTD, orms.SOTD.post_pk == orms.PostAssociation.post_pk)
                .join(orms.Post, orms.Post.pk == orms.PostAssociation.post_pk)
                .where(orms.SOTD.pk == orms.SOTDAssociation.sotd_pk)
                .where(orms.SOTDAssociation.challenge == challenge)
                .group_by(
                    orms.PostAssociation.day,
                    orms.Post.pk,
                    orm_type.brand,
                )
                .order_by(text("day, count DESC"))
            )

        lather_brands = sel(orms.WSDBLather)
        postshave_brands = sel(orms.WSDBPostshave)
        fragrance_brands = sel(orms.WSDBFragrance)

        def adapt(sel):
            outer_use = defaultdict(lambda: defaultdict(list))

            for brand, post, day_num, count in sel:
                outer_use[post][count].append(brand)
            return outer_use

        brand_usage = {
            "Lathers": adapt(lather_brands),
            "Post Shaves": adapt(postshave_brands),
            "Fragrances": adapt(fragrance_brands),
        }

        return render_template(
            "challenge/brand_stats_by_day.html",
            challenge=challenge,
            brand_usage=brand_usage,
        )
