#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   notes.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   20 Jun 2023

@brief  blueprint for displaying note categories

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from flask import Blueprint, render_template
from werkzeug.exceptions import abort

from ..db import get_db
from .. import orms

from collections import defaultdict


bp = Blueprint("notes", __name__)


@bp.route("/notes/<int:note_category_pk>")
def view(note_category_pk):
    db_session = get_db()
    with db_session.begin():
        cat = db_session.get(orms.NoteCategory, note_category_pk)
        if cat is None:
            abort(404, "this Note Category does not exist")

        notes_list = cat.scores

        notes_dict = defaultdict(list)
        for note in sorted(
            notes_list,
            key=lambda note: note.djudgement.djudgement_invitation.sotd.created_utc,
        ):
            notes_dict[note.djudgement.djudgement_invitation.sotd.post].append(note)

        return render_template("notes/view.html", category=cat, notes_dict=notes_dict)
