#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   lathergames2023.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   14 Apr 2023

@brief  orms relevant to the 2023 set of lather games rules

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from itertools import chain
from . import ChallengeBase
from .sotd_metadata import (
    MetaDataItem,
    WSDBRazor,
    WSDBBrush,
    WSDBLather,
    WSDBPostshave,
    WSDBFragrance,
)
from .score import (
    ConditionalValueCategory,
    DisqualificationCategory,
    NoteCategory,
    Category,
)
from .djudgement import DjudgementTemplate
from .selection import SelectionTemplate
from collections import namedtuple


class LatherGames2023(ChallengeBase):
    __mapper_args__ = {
        "polymorphic_identity": "LatherGames2023",
    }
    conditional_value_category_tup = namedtuple(
        "ConditionalValueCategoryTub",
        "title, guidance, default, weight, djudge_is_eligible",
    )
    conditional_value_category_tup_on_theme = namedtuple(
        "ConditionalValueCategoryTub",
        "title, guidance, default, weight, djudge_is_eligible, property_name",
    )
    dq_category_tup = namedtuple("DQCategoryTup", "title, guidance, djudge_is_eligible")
    note_category_tup = namedtuple(
        "NoteCategoryTup", "title, guidance, djudge_is_eligible, property_name"
    )

    @property
    def has_meta_data_check(self):
        "whether djudges should validate meta_data before adjudication"
        return True

    @property
    def meta_data_checks(self):
        "Which meta data items to check"
        return (WSDBRazor, WSDBBrush, WSDBLather, WSDBPostshave, WSDBFragrance)

    def fetch_or_create_category(self, db_session, cat_tup):
        category = (
            db_session.query(Category)
            .filter_by(title=cat_tup.title)
            .filter_by(challenge_pk=self.pk)
            .one_or_none()
        )
        if category is None:
            if isinstance(
                cat_tup,
                (
                    self.conditional_value_category_tup,
                    self.conditional_value_category_tup_on_theme,
                ),
            ):
                category = ConditionalValueCategory(challenge=self, **cat_tup._asdict())
            elif isinstance(cat_tup, self.dq_category_tup):
                category = DisqualificationCategory(challenge=self, **cat_tup._asdict())
            elif isinstance(cat_tup, self.note_category_tup):
                category = NoteCategory(challenge=self, **cat_tup._asdict())
            else:
                raise KeyError(
                    f"I don't know how to handle category tuple '{cat_tup}'"
                )  # pragma: no cover
        return category

    def daily_categories(self, db_session):
        """
        returns a list of categories which are djudged every day (typically everything
        other than the challenge, since any given day could have a regular, or a special
        challenge)

        Keyword Arguments:
        db_session -- sqlalchemy session object
        """
        categories = (
            self.dq_category_tup(
                title="Disqualification",
                guidance=(
                    "Explain briefly why this SOTD should be disqualified following "
                    "rule *3.4 Full Disqualifications*. ***Writing anything in this "
                    "field causes this SOTD to be disqualified.***. Leave empty for "
                    "a valid SOTD."
                ),
                djudge_is_eligible=True,
            ),
            self.conditional_value_category_tup_on_theme(
                title="Daily Theme Points",
                guidance=(
                    "Earn two points every day by lathering and shaving with a single"
                    " product that meets the daily theme's detailed criteria.  You may"
                    " use any shave soap, cream, or gel that satisfies the theme.  If"
                    " it isn't immediately obvious how your lather is supposed to be on"
                    " theme, please explain it in your SOTD writeup.\n\nSee Section 3"
                    " of the rules to find out what happens if your soap isn't on"
                    " theme."
                ),
                default=False,
                weight=2.0,
                property_name="on theme",
                djudge_is_eligible=True,
            ),
            self.conditional_value_category_tup(
                title="Relevant Post Shave & Fragrance",
                guidance=(
                    "Earn 1/5th of a point every day by keeping your lather, post"
                    " shave, and fragrance thematically linked in some way.  (E.g.,"
                    " perhaps all your software is on-theme, or there is some sort of"
                    " wordplay binding products together - Santa Noir meets Santal"
                    ' Noir, anybody?)\n\n**Note**: "trick-holing" products is by far the'
                    " simplest (yet least-creative) way to earn this point.  The judges"
                    " welcome all kinds of creativity here as long as it isn't too"
                    " far-fetched or hard to understand; if the connection between the"
                    " products is tangential at best and you neglect to explain it, you"
                    " probably won't earn this point."
                ),
                default=False,
                weight=0.2,
                djudge_is_eligible=True,
            ),
            self.conditional_value_category_tup(
                title="Topical consistency",
                guidance=(
                    "SOTD writeup is topically consistent and relevant to the "
                    "goings-on of the day; e.g., germane to the daily theme, "
                    "challenge, or running gags"
                ),
                default=False,
                weight=2.0 / 9.0,
                djudge_is_eligible=False,
            ),
            self.conditional_value_category_tup(
                title="Above and beyond",
                guidance=(
                    "SOTD writeup demonstrates that the player is clearly going"
                    " above-and-beyond the average level of effort in their LG"
                    " participation"
                ),
                default=False,
                weight=2.0 / 9.0,
                djudge_is_eligible=False,
            ),
            self.conditional_value_category_tup(
                title="Enjoyable",
                guidance=(
                    "SOTD writeup is especially enjoyable, engaging, or delightful to"
                    " read / watch"
                ),
                default=False,
                weight=2.0 / 9.0,
                djudge_is_eligible=False,
            ),
            self.conditional_value_category_tup(
                title="Offensive",
                guidance=(
                    "SOTD writeup is offensive, distasteful, against the fun spirit of"
                    " the games, or way too long without some sort of enjoyable or"
                    ' satisfying payoff; e.g., please never make a Judge say "I just'
                    " spent eighteen boring minutes watching a dude quietly shave"
                    " himself and I'll never get that time back; I think I am wasting"
                    ' my life."'
                ),
                default=False,
                weight=-2.0 / 9.0,
                djudge_is_eligible=False,
            ),
            self.note_category_tup(
                title="Podcast Candidate",
                guidance=(
                    "If you think we should talk about this post in the podcast, "
                    "please write why. (Markdown allowed)."
                ),
                property_name="note_podcast",
                djudge_is_eligible=True,
            ),
            self.note_category_tup(
                title="Shitlisted",
                guidance=(
                    "If you think this should be an EIS candidate, "
                    "please write why. (Markdown allowed)."
                ),
                property_name="excellence_in_shitposting",
                djudge_is_eligible=True,
            ),
        )

        return [self.fetch_or_create_category(db_session, c) for c in categories]

    def create_invite_regular_challenge(self, db_session):
        """
        Keyword Arguments:
        djudge -- The djudge assigned
        sotd   -- The SOTD to be djudged
        """
        categories = self.daily_categories(db_session)
        categories.append(
            self.fetch_or_create_category(
                db_session,
                self.conditional_value_category_tup(
                    title="Daily Challenge",
                    guidance=(
                        "Earn 1/5th of a point every day by completing the Daily"
                        " Challenge (announced 24 hrs beforehand).\nNote: Special"
                        " Challenge days (see next line) will not also have the surprise"
                        " Daily Challenges."
                    ),
                    default=False,
                    weight=0.2,
                    djudge_is_eligible=True,
                ),
            )
        )
        name = "DailyChallenge"
        template = DjudgementTemplate(challenge=self, categories=categories, name=name)
        self.djudgement_templates[name] = template
        return template

    def create_invite_special_challenge(self, db_session):
        """
        Keyword Arguments:
        djudge -- The djudge assigned
        sotd   -- The SOTD to be djudged
        """
        categories = self.daily_categories(db_session)
        name = "Special Challenge (1/2)"
        categories.append(
            self.fetch_or_create_category(
                db_session,
                self.conditional_value_category_tup(
                    title=name,
                    guidance=(
                        "Earn 1/2 point of a point by completing the Special"
                        " Challenges (scheduled on the calendar)."
                    ),
                    default=False,
                    weight=0.5,
                    djudge_is_eligible=True,
                ),
            )
        )

        template = DjudgementTemplate(challenge=self, categories=categories, name=name)
        self.djudgement_templates[name] = template
        return template

    def create_invite_extra_special_challenge(self, db_session):
        """
        Keyword Arguments:
        djudge -- The djudge assigned
        sotd   -- The SOTD to be djudged
        """
        categories = self.daily_categories(db_session)
        name = "Special Challenge (1/1)"
        categories.append(
            self.fetch_or_create_category(
                db_session,
                self.conditional_value_category_tup(
                    title=name,
                    guidance=(
                        "Earn 1/1 point of a point by completing the Special"
                        " Challenges (scheduled on the calendar)."
                    ),
                    default=False,
                    weight=1.0,
                    djudge_is_eligible=True,
                ),
            )
        )

        template = DjudgementTemplate(challenge=self, categories=categories, name=name)
        self.djudgement_templates[name] = template
        return template

    def create_legendary_post_template(self, db_session):
        title = "Bonus for Legendary Posts"
        self.selection_templates[title] = SelectionTemplate(
            challenge=self,
            weight=1.0,
            title=title,
            guidance=(
                "Every 6 days each Judge will review the last 6 days of posts that "
                "they have assessed and may nominate their favorite post from that "
                "period for a Legendary Post Bonus Point.  It is possible to be "
                "nominated up to five times through the month."
            ),
        )
        return self.selection_templates[title]

    def check_for_participation(self, body):
        """For lather games, every comment in the sotd thread is considered a
        participant, so  we just override the parent method and always return
        True
        """
        return True

    def handle_new_comment(
        self, session, post, comment, additional_filter=lambda x: True, override=False
    ):
        """
        parse SOTD, store relevant metadata as evidence for djudges

        Keyword Arguments:
        session -- sqlalchemy db session
        reddit  -- praw reddit instance
        comment -- praw reddit comment to check
        additional_filter -- an ad hoc filter that can additionally exclude sotds
        override -- add sotd even if participation checks fail
        """
        sotd = super().handle_new_comment(
            session, post, comment, additional_filter, override
        )
        if sotd is None:
            return None

        session.flush()
        MetaDataItem.parse_all(
            sotd=sotd,
            challenge=self,
            session=session,
            allowed_hashtag_dict={
                "razors": [
                    ht.tag
                    for ht in chain(*[h.tags for h in self.razor_scavenger_hunts])
                ],
                "brushes": [
                    ht.tag
                    for ht in chain(*[h.tags for h in self.brush_scavenger_hunts])
                ],
            },
        )
        return sotd
