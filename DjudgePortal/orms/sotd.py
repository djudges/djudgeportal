#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   sotd.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   02 Jul 2022

@brief  ORM for sotd posts

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    ForeignKey,
    UnicodeText,
    Index,
    Computed,
    TypeDecorator,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship, attribute_keyed_dict, object_session

from sqlalchemy.dialects import postgresql
from datetime import datetime
import pytz
import markdown
from markdown.extensions.tables import TableExtension
from .. import Base
from ..utils import get_user


class TSVector(TypeDecorator):
    impl = postgresql.TSVECTOR
    cache_ok = True


class Post(Base):
    __tablename__ = "posts"
    pk = Column(Integer, primary_key=True)
    day_associations = relationship(
        "PostAssociation",
        collection_class=attribute_keyed_dict("challenge"),
        viewonly=True,
    )
    sotds = relationship("SOTD")
    permalink = Column(Unicode, nullable=False, unique=True)
    title = Column(Unicode, nullable=False)
    challenges = relationship(
        "ChallengeBase",
        secondary="post_associations",
        back_populates="posts",
        viewonly=True,
    )
    challenge_associations = relationship("PostAssociation", back_populates="post")

    def fill(self, reddit):
        post = reddit.submission(url="https://reddit.com" + self.permalink)
        post.comment_sort = "new"
        post.comments.replace_more(limit=None)
        session = object_session(self)
        for comment in post.comments:
            if comment.body:
                user = get_user(object_session(self), comment.author)
                if user is not None:
                    if (
                        not session.query(SOTD)
                        .filter_by(permalink=comment.permalink)
                        .one_or_none()
                    ):
                        print(
                            f"adding SOTD by {user.user_name}, https://reddit.com{comment.permalink}"
                        )
                        self.sotds.append(
                            SOTD(
                                author=user,
                                created_utc=comment.created_utc,
                                post_pk=self.pk,
                                retrieved_on=datetime.now().timestamp(),
                                text=comment.body,
                                permalink=comment.permalink,
                            )
                        )
                    else:
                        print(f"SOTD by {user.user_name} already in db")

    def scan(self, reddit, session, additional_filter=lambda x: True):
        post = reddit.submission(url="https://reddit.com" + self.permalink)
        post.comment_sort = "new"
        post.comments.replace_more(limit=None)

        for comment in post.comments:
            if comment.body:
                if additional_filter(comment.body):
                    for challenge in self.challenges:
                        challenge.handle_new_comment(session, self, comment)


class SOTD(Base):
    __tablename__ = "sotds"
    pk = Column(Integer, primary_key=True)
    user_pk = Column(Integer, ForeignKey("users.pk"), nullable=False)
    author = relationship("User", back_populates="sotds")
    post_pk = Column(ForeignKey("posts.pk"), nullable=False)
    post = relationship("Post", back_populates="sotds")
    created_utc = Column(Integer, nullable=False)
    retrieved_on = Column(Integer, nullable=False)
    text = Column(UnicodeText, nullable=False)
    text_hash = Column(UnicodeText, Computed("MD5(text)", persisted=True))
    ts_vector_text = Column(
        TSVector, Computed("to_tsvector('english', text)", persisted=True)
    )
    __table_args__ = (
        Index("ix_ts_vector_comment_text", ts_vector_text, postgresql_using="gin"),
        UniqueConstraint("pk", "text_hash", "retrieved_on", name="_unique_sotd"),
    )
    permalink = Column(Unicode, nullable=False, unique=True)
    challenges = relationship(
        "ChallengeBase", secondary="sotd_associations", back_populates="sotds"
    )
    djudgement_invitations = relationship(
        "DjudgementInvitation", secondary="sotd_associations", viewonly=True
    )

    __meta_info = relationship(
        "SOTDMetaDataAssociation",
        secondary="sotd_associations",
        viewonly=True,
    )

    @property
    def meta_info(self):
        cat_val = {
            "Razor": 0,
            "Brush": 1,
            "Lather": 2,
            "Post Shave": 3,
            "Fragrance": 4,
        }
        return sorted(
            self.__meta_info,
            key=lambda item: (cat_val[item.item.category_name], item.pk),
        )

    def meta_for_challenge(self, challenge):
        return [mi for mi in self.meta_info if mi.challenge == challenge]

    @property
    def meta_data(self):
        return [mi.item for mi in self.meta_info]

    def __repr__(self):
        return f"SOTD({self.pk}, {self.author.user_name})"

    @property
    def markdown_text(self):
        return markdown.markdown(self.text, extensions=[TableExtension()])

    @property
    def pst_datetime(self):
        timezone = pytz.timezone("America/Los_Angeles")
        return datetime.fromtimestamp(self.created_utc, timezone)
