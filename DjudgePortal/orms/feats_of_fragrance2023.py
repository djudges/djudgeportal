#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   feats_of_fragrance2023.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   22 Jun 2023

@brief  orms relevant to the 2023 feats of fragrance rules

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


from . import KeywordChallenge

from .score import MultipleChoiceCategory, MultipleChoiceCategoryChoice

from .djudgement import DjudgementTemplate


class FeatsOfFragrance2023(KeywordChallenge):
    __mapper_args__ = {
        "polymorphic_identity": "FeatsOfFragrance2023",
    }

    def fetch_or_create_category(self, db_session):
        title = "Score"
        category = (
            db_session.query(MultipleChoiceCategory)
            .filter_by(title=title)
            .filter_by(challenge_pk=self.pk)
            .one_or_none()
        )
        if category is None:
            category = MultipleChoiceCategory(
                title=title,
                guidance="Give this SOTD a score from 0 to 5",
                challenge=self,
                choices=[
                    MultipleChoiceCategoryChoice(label=f"{val}", weight=val)
                    for val in range(6)
                ],
            )
            db_session.add(category)
            db_session.flush()
        return category

    def create_invite(self, db_session):
        name = "Invitation"
        template = DjudgementTemplate(
            challenge=self,
            categories=[self.fetch_or_create_category(db_session)],
            name=name,
        )
        self.djudgement_templates[name] = template
        return template
