#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   analysis.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   19 Sep 2022

@brief  ORM for analyses

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sqlalchemy import Column, Integer, Unicode, ForeignKey, Table, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship
from .score import TallyCategory

from collections import defaultdict, namedtuple

from .. import Base

if False:
    from . import ChallengeBase, User

_ = Table(
    "challenge_association",
    Base.metadata,
    Column("challenge_pk", ForeignKey("challenge_bases.pk"), primary_key=True),
    Column("analysis_pk", ForeignKey("analyses.pk"), primary_key=True),
)


class SOTDPointsTally(Base):
    __tablename__ = "sotd_points_tallies"
    pk: Mapped[int] = mapped_column(primary_key=True)
    category_pk: Mapped[int] = mapped_column(ForeignKey("tally_categories.pk"))
    category: Mapped["TallyCategory"] = relationship(back_populates="tallies")
    value: Mapped[float]
    challenge_pk: Mapped[int] = mapped_column(ForeignKey("challenge_bases.pk"))
    challenge: Mapped["ChallengeBase"] = relationship(
        back_populates="sotd_points_tallies"
    )
    user_pk: Mapped[int] = mapped_column(ForeignKey("users.pk"))
    user: Mapped["User"] = relationship(back_populates="sotd_points_tallies")
    __table_args__ = (
        UniqueConstraint(
            "category_pk", "challenge_pk", "user_pk", name="_unique_tally"
        ),
    )

    tally_type: Mapped[str] = mapped_column(nullable=False)
    scores = relationship(
        "SOTDTallyScore",
        secondary=(
            "join(SOTDTallyScore, Djudgement,"
            "     SOTDTallyScore.djudgement_pk == Djudgement.pk)"
            ".join(DjudgementInvitation, "
            "      Djudgement.djudgement_invitation_pk == DjudgementInvitation.pk)"
            ".join(SOTDAssociation, "
            "      DjudgementInvitation.sotd_association_pk == SOTDAssociation.pk)"
        ),
        primaryjoin=(
            "and_(SOTDPointsTally.user_pk==Djudgement.user_pk, "
            "     SOTDPointsTally.challenge_pk == SOTDAssociation.challenge_pk,"
            "     ConditionalValueScore.category_pk == SOTDPointsTally.category_pk)"
        ),
        viewonly=True,
    )

    __mapper_args__ = {
        "polymorphic_identity": "SOTDPointsTally",
        "polymorphic_on": tally_type,
        "with_polymorphic": "*",
    }

    def __repr__(self):
        return f"Tally({self.challenge.name}::{self.category.title})"

    @property
    def name(self):
        return self.category.title

    def update(self):
        self.value = sum((score.num_val for score in self.scores))
        return self.value


class AnalysisBase(Base):
    __tablename__ = "analyses"
    pk = Column(Integer, primary_key=True)
    challenges = relationship(
        "ChallengeBase", secondary="challenge_association", back_populates="analyses"
    )
    analysis_type = Column(Unicode, nullable=False)

    __mapper_args__ = {
        "polymorphic_identity": "Base",
        "polymorphic_on": analysis_type,
        "with_polymorphic": "*",
    }


class SideSideChallengeAnalysis(AnalysisBase):
    name = "SideSideChallenge"
    __tablename__ = "side_side_challenges"
    pk = Column(Integer, ForeignKey("analyses.pk"), primary_key=True)
    __mapper_args__ = {"polymorphic_identity": name}

    @property
    def ranking(self):
        scores = defaultdict(float)
        invitation_counter = defaultdict(int)
        djudgement_counter = defaultdict(int)

        def numeric_score(djudgement):
            valid = True
            score_val = 0.0
            for score in djudgement.scores:
                if score.score_type == "BooleanScore":
                    if "DQ" in score.category.title:
                        if score.value:
                            valid = False
                if score.score_type == "NumericScore":
                    score_val += score.value if score.value is not None else 0.0
            return score_val if valid else 0.0

        for challenge in self.challenges:
            for inv in challenge.djudgement_invitations:
                user = inv.sotd.author
                invitation_counter[user] += 1
                if inv.djudgement is not None:
                    djudgement_counter[user] += 1
                    scores[user] += numeric_score(inv.djudgement)

        row = namedtuple("row", "user score ratio")
        return sorted(
            (
                row(
                    user,
                    scores[user],
                    djudgement_counter[user] / max(1, invitation_counter[user]),
                )
                for user in scores
            ),
            key=lambda t: t.score,
            reverse=True,
        )
