#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_user.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   23 Aug 2022

@brief  tests for users and their representation

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from DjudgePortal import orms, load_user
from DjudgePortal.db import get_db


def test_user(client_with_keyword_challenges, auth):
    db_session = get_db()
    with db_session.begin():
        djudge1 = db_session.query(orms.User).filter_by(user_name="djudge 1").one()
        djudge2 = db_session.query(orms.User).filter_by(user_name="djudge 2").one()
        assert len(djudge1.docket) == 2
        assert len(djudge2.docket) == 0
        # the same sotd is part of two challenges
        assert djudge1.docket[0].sotd == djudge1.docket[1].sotd
        assert djudge1.docket[0] != djudge1.docket[1]

    with db_session.begin():
        pass  # orms.DjudgementInvitation(


def test_load_user_in_transaction(client):
    db_session = get_db()
    with db_session.begin():
        user = load_user("1")
        assert user.user_name == "admin"


def test_load_user_no_transaction(client):
    user = load_user("1")

    db_session = get_db()
    with db_session.begin():
        assert user.user_name == "admin"
