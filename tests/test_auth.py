#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_auth.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   21 Jul 2022

@brief  Tests for authentication and login/logout

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from flask import g, session as flask_session, current_app
from DjudgePortal import orms
from DjudgePortal.db import get_db

from datetime import datetime


def test_login(client, app_with_people):
    with client:
        assert client.get("/auth/login").status_code == 200
    db_session = get_db()
    with db_session.begin():
        user_name = app_with_people.config["ROOT_ADMIN"]
        user = db_session.query(orms.User).filter_by(user_name=user_name).one()
        access_code = user.new_access_code()
    response = client.post(
        "/auth/login",
        data={"user_name": user_name, "access_token": access_code},
        follow_redirects=True,
    )
    login_check = ("<li><span>&nbsp; logged in as administrator ").encode()

    assert login_check in response.data

    with client:
        _ = client.get("/index")
        assert flask_session["_user_id"] == str(1)


@pytest.mark.parametrize(
    ("user_name", "password", "message"),
    (
        ("a", 123, b"is not an organiser of, nor djudge or participant"),
        (None, 1023, b"Incorrect access code."),
    ),
)
def test_login_validate_input(auth, user_name, password, message):
    if user_name is None:
        user_name = current_app.config["ROOT_ADMIN"]
    response = auth.login(user_name, password)
    assert message in response.data


def test_request_an_access_code(auth):
    db_session = get_db()
    with db_session.begin():
        user_name = "temp"
        user = orms.User(user_name=user_name)
        db_session.add(user)
    response = auth.login(user_name, "aaut")
    assert b"You haven&#39;t requested an access code for user" in response.data


def test_expired_access_code(auth):
    db_session = get_db()
    with db_session.begin():
        user_name = "temp"
        user = orms.User(user_name=user_name)
        user.new_access_code()
        user.access_code_valid_until = int(datetime.now().timestamp() - 1)
        db_session.add(user)
    response = auth.login(user_name, "aaut")
    assert b"access code has expired" in response.data


def test_logout(client, auth):
    auth.login()

    with client:
        auth.logout()
        assert "user_id" not in flask_session

        assert "user" not in g
