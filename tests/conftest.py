#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   conftest.py

@author Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   19 Jul 2022

@brief  config and initial population of the database for testing

Copyright © 2022 Djundjila

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import pytest
from DjudgePortal import create_app, Base, orms
from DjudgePortal.db import get_db, init_db

from flask import current_app, g
from sqlalchemy.orm import close_all_sessions

from unittest.mock import MagicMock


@pytest.fixture
def empty_app():
    test_config = None
    if os.environ.get("RUN_IN_CI") == "1":
        test_config = dict(
            SECRET_KEY="dev",
            SQLALCHEMY_DATABASE_URI=os.environ.get("DATABASE_URL"),
            FLASK_ENV="development",
            ENV="development",
            TESTING=True,
            ROOT_ADMIN="admin",
            REDDIT_IDENTITY_BOT="RedditBot",
            ACCESS_CODE_SIZE=1000000,
            ACCESS_CODE_TTL=600,
            WTF_CSRF_ENABLED=False,
        )

    app = create_app(test_config, dot_env_file=".env_test")
    with app.app_context():
        db_session = get_db()
        Base.metadata.drop_all(db_session.get_bind())
        init_db()

        yield app

        close_all_sessions()
        Base.metadata.drop_all(db_session.get_bind())


@pytest.fixture
def app_with_people(empty_app):
    db_session = get_db()
    with db_session.begin():
        admin = orms.User(user_name=empty_app.config["ROOT_ADMIN"], is_admin=True)
        admin.new_access_code()
        db_session.add(admin)
        db_session.flush()
        assert db_session.query(orms.User).count() == 1

        owner = orms.User(user_name="owner")
        db_session.add(owner)

        participant1 = orms.User(user_name="participant 1")
        db_session.add(participant1)

        participant2 = orms.User(user_name="participant 2")
        db_session.add(participant2)

        participant3 = orms.User(user_name="participant 3")
        db_session.add(participant3)

        djudge1 = orms.User(user_name="djudge 1")
        db_session.add(djudge1)

        djudge2 = orms.User(user_name="djudge 2")
        db_session.add(djudge2)
        db_session.flush()
        g.owner_pk = owner.pk
        g.participant1_pk = participant1.pk
        g.participant2_pk = participant2.pk
        g.participant3_pk = participant3.pk
        g.djudge1_pk = djudge1.pk
        g.djudge2_pk = djudge2.pk

    return empty_app


@pytest.fixture
def app_with_keyword_challenges(app_with_people):
    db_session = get_db()
    with db_session.begin():
        owner = db_session.get(orms.User, g.owner_pk)
        challenge1 = orms.KeywordChallenge(
            owners=[owner],
            pk=101,
            name="challenge 1",
            title="challenge 1 title",
            post_url=(
                "https://www.reddit.com/r/Wetshaving/comments/woru5m"
                "/monday_austere_august_sotd_thread_aug_15_2022"
            ),
            search_string="hashtag1#",
            description="describing\ntext",
        )
        db_session.add(challenge1)

        challenge2 = orms.KeywordChallenge(
            owners=[owner],
            pk=102,
            name="challenge 2",
            title="challenge 2 title",
            post_url=(
                "https://www.reddit.com/r/Wetshaving/comments/we3sxf/"
                "tuesday_austere_august_sotd_thread_aug_02_2022"
            ),
            search_string="hashtag2#",
            description="describing\ntext 2",
        )
        db_session.add(challenge2)

        post_day_1 = orms.Post(
            permalink="https://post1",
            title="Day1",
        )
        assoc1 = orms.PostAssociation(challenge=challenge1, post=post_day_1, day=1)
        assoc2 = orms.PostAssociation(challenge=challenge2, post=post_day_1, day=1)
        db_session.add_all((post_day_1, assoc1, assoc2))

        post_day_2 = orms.Post(
            permalink="https://post2",
            title="Day2",
        )
        assoc1 = orms.PostAssociation(challenge=challenge1, post=post_day_2, day=2)
        assoc2 = orms.PostAssociation(challenge=challenge2, post=post_day_2, day=2)
        db_session.add(post_day_2)

        participant1 = db_session.get(orms.User, g.participant1_pk)
        participant2 = db_session.get(orms.User, g.participant2_pk)
        participant3 = db_session.get(orms.User, g.participant3_pk)
        sotd_1_both_ch = orms.SOTD(
            author=participant1,
            post=post_day_1,
            created_utc=1,
            retrieved_on=101,
            text="\n".join((challenge1.search_string, challenge2.search_string)),
            permalink="https://sotd_1_both_ch",
        )
        db_session.add(sotd_1_both_ch)

        sotd_2_only_ch1 = orms.SOTD(
            author=participant2,
            post=post_day_1,
            created_utc=2,
            retrieved_on=201,
            text=challenge1.search_string,
            permalink="https://sotd_2_only_ch1",
        )
        db_session.add(sotd_2_only_ch1)

        sotd_3_only_ch2 = orms.SOTD(
            author=participant3,
            post=post_day_2,
            created_utc=1001,
            retrieved_on=1201,
            text=challenge2.search_string,
            permalink="https://sotd_3_only_ch2",
        )
        db_session.add(sotd_3_only_ch2)

        sotd_4_both_ch = orms.SOTD(
            author=participant1,
            post=post_day_2,
            created_utc=1002,
            retrieved_on=1202,
            text="\n".join((challenge1.search_string, challenge2.search_string)),
            permalink="https://sotd_4_both_ch",
        )
        db_session.add(sotd_4_both_ch)

        for sotd in (
            sotd_1_both_ch,
            sotd_2_only_ch1,
            sotd_3_only_ch2,
            sotd_4_both_ch,
        ):
            for challenge in (challenge1, challenge2):
                if challenge.check_for_participation(sotd.text):
                    challenge.sotds.append(sotd)

        db_session.flush()
        assoc = challenge1.sotd_associations[sotd_1_both_ch]
        djudge1 = db_session.get(orms.User, g.djudge1_pk)
        assoc.djudge = djudge1
        invite1 = orms.DjudgementInvitation(sotd_association=assoc)
        db_session.add(invite1)

        assoc = challenge2.sotd_associations[sotd_1_both_ch]
        assoc.djudge = djudge1
        invite2 = orms.DjudgementInvitation(sotd_association=assoc)
        db_session.add(invite2)
        db_session.flush()
        g.challenge_pks = (challenge1.pk, challenge2.pk)
        g.sotd_pks = (
            sotd_1_both_ch.pk,
            sotd_2_only_ch1.pk,
            sotd_3_only_ch2.pk,
            sotd_4_both_ch.pk,
        )

    return app_with_people


@pytest.fixture
def app_with_lg_challenge(app_with_people):
    db_session = get_db()
    with db_session.begin():
        owner = db_session.get(orms.User, g.owner_pk)
        lg = orms.LatherGames2023(
            owners=[owner],
            name="lg_test",
            title="lg_test_title",
            description="lg_test_description",
            post_url=(
                "https://www.reddit.com/r/Wetshaving/comments/woaoeuaeuruom"
                "/monday_austere_august_sotd_thread_aug_15_2022"
            ),
        )
        db_session.add(lg)
        djudge1 = db_session.query(orms.User).filter_by(user_name="djudge 1").one()
        djudge2 = db_session.query(orms.User).filter_by(user_name="djudge 2").one()
        lg.djudges += [djudge1, djudge2]
        razor_scavenger_hunt = orms.ScavengerHunt(
            challenge=lg,
            title="Razor #Hunt",
            metadata_type="razor",
            weight=1.2,
            max_score=2.0,
        )
        razor_scavenger_hunt.tags.append(orms.WSDBHashTag(tag="#CHROME"))
        brush_scavenger_hunt = orms.ScavengerHunt(
            challenge=lg,
            title="Brush #Hunt",
            metadata_type="razor",
            weight=1.2,
            max_score=2.0,
        )
        brush_scavenger_hunt.tags.append(orms.WSDBHashTag(tag="#TANGLED"))
        lg.scavenger_hunts.append(razor_scavenger_hunt)
        lg.scavenger_hunts.append(brush_scavenger_hunt)
        db_session.flush()
        g.lg_pk = lg.pk

    return app_with_people


@pytest.fixture
def app_with_fof_challenge(app_with_people):
    db_session = get_db()
    with db_session.begin():
        owner = db_session.get(orms.User, g.owner_pk)
        fof = orms.FeatsOfFragrance2023(
            owners=[owner],
            name="fof_test",
            title="fof_test_title",
            description="fof_test_description",
            post_url=(
                "https://www.reddit.com/r/Wetshaving/comments/woaoeuaeuruom"
                "/monday_austere_august_sotd_thread_aug_15_2022astoehu"
            ),
            search_string="#FOF",
        )
        db_session.add(fof)

    with db_session.begin():
        djudge1 = db_session.query(orms.User).filter_by(user_name="djudge 1").one()
        djudge2 = db_session.query(orms.User).filter_by(user_name="djudge 2").one()
        fof.djudges += [djudge1, djudge2]
        g.fof_pk = fof.pk

        post = orms.Post(
            permalink="https://post1",
            title="Day1",
        )
        assoc = orms.PostAssociation(challenge=fof, post=post, day=1)
        db_session.add_all((post, assoc))
        db_session.flush()

        reddit_comment = MagicMock()
        reddit_comment.permalink = "test_perma/link"
        reddit_comment.author = MagicMock()
        reddit_comment.author.name = "authorname"
        reddit_comment.created_utc = 12
        reddit_comment.body = """* **Razor:** El Razoro #CHROME
* **Brush:** El Brusho #HANDLE
\\#FOF"""

        sotd = fof.handle_new_comment(db_session, post, reddit_comment)
        db_session.flush()
        g.sotd_pk = sotd.pk

    return app_with_people


@pytest.fixture
def app_with_2_lg_challenges(app_with_people):
    db_session = get_db()
    with db_session.begin():
        owner = db_session.get(orms.User, g.owner_pk)
        for i in range(1, 3):
            lg = orms.LatherGames2023(
                owners=[owner],
                name=f"lg_test{i}",
                title=f"lg_test_title{i}",
                description=f"lg_test_description{i}",
                post_url=(
                    "https://www.reddit.com/r/Wetshaving/comments/woaoeuaeuruom"
                    f"/monday_austere_august_sotd_thread_aug_15_2022&{i}"
                ),
            )
            db_session.add(lg)
            djudge1 = db_session.query(orms.User).filter_by(user_name="djudge 1").one()
            djudge2 = db_session.query(orms.User).filter_by(user_name="djudge 2").one()
            lg.djudges += [djudge1, djudge2]
        razor_scavenger_hunt = orms.ScavengerHunt(
            challenge=lg,
            title="Razor #Hunt",
            metadata_type="razor",
            weight=1.2,
            max_score=2.0,
        )
        razor_scavenger_hunt.tags.append(orms.WSDBHashTag(tag="#CHROME"))
        brush_scavenger_hunt = orms.ScavengerHunt(
            challenge=lg,
            title="Brush #Hunt",
            metadata_type="razor",
            weight=1.2,
            max_score=2.0,
        )
        brush_scavenger_hunt.tags.append(orms.WSDBHashTag(tag="#TANGLED"))
        lg.scavenger_hunts.append(razor_scavenger_hunt)
        lg.scavenger_hunts.append(brush_scavenger_hunt)

    return app_with_people


@pytest.fixture
def lg_challenge_with_sotd(app_with_lg_challenge):
    db_session = get_db()
    with db_session.begin():
        lg = db_session.get(orms.LatherGames2023, 1)
        post = orms.Post(
            permalink="https://post1",
            title="Day1",
        )
        assoc = orms.PostAssociation(challenge=lg, post=post, day=1)
        db_session.add_all((post, assoc))
        db_session.flush()

        reddit_comment = MagicMock()
        reddit_comment.permalink = "test_perma/link"
        reddit_comment.author = MagicMock()
        reddit_comment.author.name = "authorname"
        reddit_comment.created_utc = 12
        reddit_comment.body = """* **Razor:** El Razoro #CHROME
* **Brush:** El Brusho #HANDLE"""

        sotd = lg.handle_new_comment(db_session, post, reddit_comment)
        db_session.flush()
        g.sotd_pk = sotd.pk

    return app_with_lg_challenge


@pytest.fixture
def lg_challenge_with_2_sotd_2_lg(app_with_2_lg_challenges):
    db_session = get_db()
    with db_session.begin():
        g.lg_pks = list()
        g.sotd_pks = (list(), list())
        g.author_ids = set()
        for lg_id in range(1, 3):
            lg = db_session.get(orms.LatherGames2023, lg_id)
            post = orms.Post(
                permalink=f"https://post{lg_id}",
                title="Day1",
            )
            assoc = orms.PostAssociation(challenge=lg, post=post, day=1)
            db_session.add_all((post, assoc))
            db_session.flush()
            g.lg_pks.append(lg.pk)

            for author_id in range(1, 3):
                reddit_comment = MagicMock()
                reddit_comment.permalink = f"test_perma/link_{author_id}"
                reddit_comment.author = MagicMock()
                reddit_comment.author.name = f"author_{author_id}"
                reddit_comment.created_utc = 12
                reddit_comment.body = """* **Razor:** El Razoro #CHROME
        * **Brush:** El Brusho #HANDLE"""

                sotd = lg.handle_new_comment(db_session, post, reddit_comment)
                db_session.flush()

                g.author_ids.add(sotd.author.pk)
                g.sotd_pks[lg_id - 1].append(sotd.pk)

    return app_with_lg_challenge


@pytest.fixture
def lg_challenge_with_2_sotd_2_lg_legacy(app_with_2_lg_challenges):
    db_session = get_db()
    with db_session.begin():
        g.lg_pks = list()
        g.sotd_pks = (list(), list())
        g.author_ids = set()
        for lg_id in range(1, 3):
            lg = db_session.get(orms.LatherGames2023, lg_id)
            post = orms.Post(
                permalink=f"https://post{lg_id}",
                title="Day1",
            )
            assoc = orms.PostAssociation(challenge=lg, post=post, day=1)
            db_session.add_all((post, assoc))
            db_session.flush()
            g.lg_pks.append(lg.pk)

            for author_id in range(1, 3):
                reddit_comment = MagicMock()
                reddit_comment.permalink = "WRONG_REPEATED_STATIC_URL"
                reddit_comment.author = MagicMock()
                reddit_comment.author.name = f"author_{author_id}"
                reddit_comment.created_utc = 12
                reddit_comment.body = """* **Razor:** El Razoro #CHROME
        * **Brush:** El Brusho #HANDLE"""

                sotd = lg.handle_new_comment(db_session, post, reddit_comment)
                db_session.flush()

                g.author_ids.add(sotd.author.pk)
                g.sotd_pks[lg_id - 1].append(sotd.pk)

    return app_with_lg_challenge


@pytest.fixture
def lg_challenge_with_many_sotd_post_2_lg(app_with_2_lg_challenges):
    db_session = get_db()
    with db_session.begin():
        g.lg_pks = list()
        g.sotd_pks = (list(), list())
        lgs = list()
        for lg_id in range(1, 3):
            lgs.append(db_session.get(orms.LatherGames2023, lg_id))
            g.lg_pks.append(lg_id)

        posts = list()
        for day in range(1, 5):
            post = orms.Post(
                permalink=f"https://post{day}",
                title=f"Day{day}",
            )

            assocs = [
                orms.PostAssociation(challenge=lg, post=post, day=day) for lg in lgs
            ]
            db_session.add_all([post] + assocs)
            posts.append(post)

        db_session.flush()
        for lg in lgs:
            for post in posts:
                for author_id in range(1, 3):
                    reddit_comment = MagicMock()
                    reddit_comment.permalink = (
                        f"test_perma/link{lg.pk}.{post.pk}.{author_id}"
                    )
                    reddit_comment.author = MagicMock()
                    reddit_comment.author.name = f"author_{author_id}"
                    reddit_comment.created_utc = 12
                    reddit_comment.body = """* **Razor:** El Razoro #CHROME
                    * **Brush:** El Brusho #HANDLE"""

                    sotd = lg.handle_new_comment(db_session, post, reddit_comment)
                    db_session.flush()
                    g.sotd_pks[lg.pk - 1].append(sotd.pk)

    return app_with_lg_challenge


@pytest.fixture
def lg_challenge_with_incomplete_djudgement(lg_challenge_with_sotd):
    db_session = get_db()
    with db_session.begin():
        # invite djudgement
        djudge = db_session.get(orms.User, g.djudge1_pk)
        lg = db_session.get(orms.ChallengeBase, g.lg_pk)
        sotd = db_session.get(orms.SOTD, g.sotd_pk)
        template = lg.create_invite_regular_challenge(db_session=db_session)
        campaign = orms.DjudgementCampaign(
            template=template,
            instructions=(
                "Today's theme is topical and the challenge is " "challenging"
            ),
        )
        invitation = campaign.invite_sotd(djudge=djudge, sotd=sotd)
        db_session.add_all((invitation, campaign, template))
        db_session.flush()
        assert invitation.state == orms.States.Unacknowledged
        assert invitation.pk is not None

        # djudge sotd
        djudgement = orms.Djudgement(djudgement_invitation=invitation)
        djudgement.scores.append(
            lg.categories["Disqualification"].new_score(value="", djudgement=djudgement)
        )
        djudgement.scores.append(
            lg.categories["Above and beyond"].new_score(
                value=True, djudgement=djudgement
            )
        )
        db_session.add(djudgement)
        db_session.flush()

        assert db_session.query(orms.Djudgement).count() == 1

        db_session.flush()
        g.djudgement_pk = djudgement.pk

    return lg_challenge_with_sotd


@pytest.fixture
def lg_challenge_with_djudgement(lg_challenge_with_incomplete_djudgement):
    db_session = get_db()
    with db_session.begin():
        djudgement = db_session.get(orms.Djudgement, g.djudgement_pk)
        lg = db_session.get(orms.ChallengeBase, g.lg_pk)
        # complete the djudgement
        djudgement.scores.append(
            lg.categories["Topical consistency"].new_score(
                value=True, djudgement=djudgement
            )
        )

        djudgement.scores.append(
            lg.categories["Enjoyable"].new_score(value=True, djudgement=djudgement)
        )

        djudgement.scores.append(
            lg.categories["Offensive"].new_score(value=False, djudgement=djudgement)
        )
        djudgement.scores.append(
            lg.categories["Daily Theme Points"].new_score(
                value=False, djudgement=djudgement
            )
        )
        djudgement.scores.append(
            lg.categories["Relevant Post Shave & Fragrance"].new_score(
                value=False, djudgement=djudgement
            )
        )
        djudgement.scores.append(
            lg.categories["Daily Challenge"].new_score(
                value=False, djudgement=djudgement
            )
        )

    return lg_challenge_with_incomplete_djudgement


@pytest.fixture
def client(app_with_people):
    return app_with_people.test_client()


@pytest.fixture
def client_with_keyword_challenges(app_with_keyword_challenges):
    return app_with_keyword_challenges.test_client()


@pytest.fixture
def runner(empty_app):
    return empty_app.test_cli_runner()


class AuthActions(object):
    def __init__(self, client):
        self._client = client

    def login_admin(self):
        db_session = get_db()
        with db_session.begin():
            user = (
                db_session.query(orms.User)
                .filter_by(user_name=current_app.config["ROOT_ADMIN"])
                .one()
            )
            username = user.user_name
            access_code = user.new_access_code()

        return self._client.post(
            "/auth/login", data={"user_name": username, "access_token": access_code}
        )

    def login(self, username=None, access_code=None):
        if username is None:
            db_session = get_db()
            with db_session.begin():
                user = db_session.query(orms.User).filter_by(user_name="owner").one()
                username = user.user_name
                access_code = user.new_access_code()

        return self._client.post(
            "/auth/login", data={"user_name": username, "access_token": access_code}
        )

    def logout(self):
        return self._client.get("/auth/logout")


@pytest.fixture
def auth(client):
    return AuthActions(client)


@pytest.fixture
def auth_with_keyword_challenges(client_with_keyword_challenges):
    return AuthActions(client_with_keyword_challenges)
