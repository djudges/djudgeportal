#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_sotd_metadata.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   22 Apr 2023

@brief  tests for sotd metadata classes

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

tts2backend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

tts2backend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from DjudgePortal import orms
from DjudgePortal.db import get_db

import pytest


@pytest.mark.parametrize(
    "line, expected_output",
    [
        (
            "* **Razor:**  Gillette Red Tip #OLD #NEW",
            ("Gillette Red Tip", ("#OLD", "#NEW")),
        ),
        (
            "* **Razor:**  [Gillette Red Tip](link.url) #Old #new",
            ("Gillette Red Tip", ("#Old", "#new")),
        ),
        (
            "* **Razor:** Gillette Red Tip #OLD#NEW",
            ("Gillette Red Tip", ("#OLD", "#NEW")),
        ),
        (
            "* **Razor:** Gillette Red Tip #OlD#",
            ("Gillette Red Tip", ("#OlD",)),
        ),
        (
            "* **Razor:** Gillette Red Tip",
            ("Gillette Red Tip", ()),
        ),
        (
            "* **Razob:** Gillette Red Tip",
            None,
        ),
    ],
)
def test_razor_parser(line, expected_output):
    parsed = orms.WSDBRazor.parse(line)
    assert parsed == expected_output


@pytest.mark.parametrize(
    "line, expected_output",
    [
        (
            "* **Brush:**  Zenith 506U XSE #UNBLEACHED #WOOD",
            ("Zenith 506U XSE", ("#UNBLEACHED", "#WOOD")),
        ),
        (
            "* **Brush:** Dogwood Handcrafts Arcane Abyss w/ Maggard Mixed Badger/Boar 26mm #IMPURE  ",
            (
                "Dogwood Handcrafts Arcane Abyss w/ Maggard Mixed Badger/Boar 26mm",
                ("#IMPURE",),
            ),
        ),
        (
            "* **Brush:** Unbranded Pure badger brush  #WOODEN #PURE",
            ("Unbranded Pure badger brush", ("#WOODEN", "#PURE")),
        ),
    ],
)
def test_brush_parser(line, expected_output):
    parsed = orms.WSDBBrush.parse(line)
    assert parsed == expected_output


@pytest.mark.parametrize(
    "line, expected_output",
    [
        (
            "* **Lather:**  Barrister and Mann - Seville",
            ("Barrister and Mann", "Seville", ()),
        ),
        (
            "* **Lather:** [St. James of London- Sandalwood & Bergamot Cream](https://trythatsoap.com/collection/774/)",
            ("St. James of London", "Sandalwood & Bergamot Cream", ()),
        ),
        (
            "* **Lather:** Black Ship Grooming - Queen Anne's Revenge",
            ("Black Ship Grooming", "Queen Anne's Revenge", ()),
        ),
        (
            "* **Lather:** Summer Break Soaps - Cannonball!",
            ("Summer Break Soaps", "Cannonball!", ()),
        ),
        (
            "Lather: Catie’s Bubbles – Connecticut Shade",
            ("Catie’s Bubbles", "Connecticut Shade", ()),
        ),
    ],
)
def test_lather_parser(line, expected_output):
    parsed = orms.WSDBLather.parse(line)
    assert parsed == expected_output


@pytest.mark.parametrize(
    "line, expected_output",
    [
        (
            "* **Post Shave:**  Barrister and Mann - Seville",
            ("Barrister and Mann", "Seville", ()),
        ),
        (
            "* **Post Shave:**  Barrister and Mann – Cheshire",
            ("Barrister and Mann", "Cheshire", ()),
        ),
        ("* **Post Shave:**  Barrister and Mann Seville", None),
        (
            "* **post shave:** [Nivea - Sensitive Post Shave Balm - Aftershave](https://trythatsoap.com/collection/2150/?product_type=aftershave)",
            ("Nivea", "Sensitive Post Shave Balm", ()),
        ),
        (
            "* **Post Shave:** [Declaration Grooming - Mayflower - Aftershave](https://trythatsoap.com/collection/700/?product_type=aftershave2)",
            ("Declaration Grooming", "Mayflower", ()),
        ),
    ],
)
def test_postshave_parser(line, expected_output):
    parsed = orms.WSDBPostshave.parse(line)
    assert parsed == expected_output


@pytest.mark.parametrize(
    "line, expected_output",
    [
        (
            "* **Fragrance:**  Barrister and Mann - Seville",
            ("Barrister and Mann", "Seville", ()),
        ),
        ("* **Fragrance:**  Barrister and Mann Seville", None),
        (
            "* **Fragrance:** Diptyque - L&#39;Ombre Dans L&#39;Eau edp",
            ("Diptyque", "L'Ombre Dans L'Eau edp", ()),
        ),
    ],
)
def test_fragrance_parser(line, expected_output):
    parsed = orms.WSDBFragrance.parse(line)
    assert parsed == expected_output


@pytest.mark.parametrize(
    "sotd_body, expected_output, allowed_hashtags, expected_hashtags",
    [
        (
            """**[June 4, 2022 - Drug Store Day](https://i.imgur.com/Sk9p31G.jpg)**

* **Prep:** Cold Water
* **Brush:** My Hands #STANKY #STAKNY
* **Razor:** Chedraui Disposable Razor #Plastic

* **Lather:** Avenè - Gel de Afeitar

* **Post Shave:** [Nivea - Sensitive Post Shave Balm - Aftershave](https://trythatsoap.com/collection/2150/?product_type=aftershave)


* **Fragrance:** Brut Cologne

This set up""",
            dict(
                razors="[Razor(Chedraui Disposable Razor)]",
                brushes="[Brush(My Hands)]",
                lathers="[Lather(Avenè - Gel de Afeitar)]",
                fragrances="[]",
                postshaves="[Post Shave(Nivea - Sensitive Post Shave Balm)]",
            ),
            dict(razors=["#PLASTIC"], brushes=["#STANKY"]),
            dict(razors=["#PLASTIC"], brushes=["#STANKY"]),
        ),
        (
            """**[June 4, 2022 - Drug Store Day](https://i.imgur.com/Sk9p31G.jpg)**

* **Prep:** Cold Water
* **Brush:** My Hands #STANKY #STAKNY
* **Razor:** Chedraui Disposable Razor #PLASTIC
* **Razor:** Murker 37X

* **Lather:** Avenè - Gel de Afeitar
* **Lather:** Mauser & Kurtz - Handbag

* **Post Shave:** [Nivea - Sensitive Post Shave Balm - Aftershave](https://trythatsoap.com/collection/2150/?product_type=aftershave)


* **Fragrance:** Brut Cologne

This set up""",
            dict(
                razors="[Razor(Chedraui Disposable Razor), Razor(Murker 37X)]",
                brushes="[Brush(My Hands)]",
                lathers="[Lather(Avenè - Gel de Afeitar), Lather(Mauser & Kurtz - Handbag)]",
                fragrances="[]",
                postshaves="[Post Shave(Nivea - Sensitive Post Shave Balm)]",
            ),
            dict(razors=["#PLASTIC"], brushes=["#STANKY"]),
            dict(razors=["#PLASTIC"], brushes=["#STANKY"]),
        ),
        (
            """July 12, 2023 – Lather Games: Day 12

Brush: Yaqi Synthetic

Razor: Karve Overlander

Blade: Gillette Nacet

Lather: Catie’s Bubbles – Connecticut Shade

Post Shave: Catie’s Bubbles – Connecticut Shade

Connecticut Shade was one of the first soaps I bought when I started wet shaving. I had read about it on a some website that stated Catie’s Bubbles was a top tier brand. I was not disappointed. The lather was creamy and slick, and it smelled great. I still find it to be top tier, but now the competition is much fiercer.

Yo meet today’s challenge, I lathered the soap on my face instead of in a bowl. I also applied it with my left hand. The result was an under performing mess on my face. At first it wasn’t wet enough, but I added too much water to compensate. I then tried adding more soap, but this put me almost to square one. I finally said screw it, and just finished shaving. I’ve had better days shaving. """,
            dict(
                razors="[Razor(Karve Overlander)]",
                brushes="[Brush(Yaqi Synthetic)]",
                lathers="[Lather(Catie’s Bubbles - Connecticut Shade)]",
                fragrances="[]",
                postshaves="[Post Shave(Catie’s Bubbles - Connecticut Shade)]",
            ),
            dict(),
            dict(),
        ),
    ],
)
def test_parse_all(
    app_with_keyword_challenges,
    sotd_body,
    expected_output,
    allowed_hashtags,
    expected_hashtags,
):
    db_session = get_db()
    with db_session.begin():
        author = db_session.get(orms.User, 1)
        post = db_session.get(orms.Post, 1)
        challenge = db_session.query(orms.ChallengeBase)[:1][0]

        stakny = orms.WSDBHashTag(tag="#STAKNY")  # hashtag that shouldn't match
        db_session.add(stakny)
        sotd = orms.SOTD(
            author=author,
            post=post,
            created_utc=3,
            retrieved_on=4,
            text=sotd_body,
            permalink="https://lin.k",
        )
        challenge.sotds.append(sotd)
    with db_session():
        assert sotd.pk is not None
        parsed = orms.WSDBBrush.parse_all(
            sotd=sotd,
            challenge=challenge,
            session=db_session,
            allowed_hashtag_dict=allowed_hashtags,
        )
        db_session.flush()

        assert stakny not in parsed["brushes"][0].hashtags
        assert stakny.__repr__() not in {f"{t}" for t in parsed["brushes"][0].hashtags}

        for key, val in expected_output.items():
            assert val == [md.item for md in parsed[key]].__repr__()

        for itemlist in parsed.values():
            if itemlist:
                assert not itemlist[0].djudicial_dq
            for item in itemlist[1:]:
                assert item.djudicial_dq

        for key, val in expected_hashtags.items():
            for tag in val:
                assert tag in {f"{t.tag}" for t in parsed[key][0].hashtags}

        assert set(expected_output.keys()) == set(parsed.keys())

        # do it (with different author) again to check that no duplicates get created
        author = db_session.get(orms.User, 2)
        sotd = orms.SOTD(
            author=author,
            post=post,
            created_utc=3,
            retrieved_on=4,
            text=sotd_body,
            permalink="https://new.lin.k",
        )
        challenge.sotds.append(sotd)
        db_session.flush()
        parsed = orms.WSDBBrush.parse_all(
            sotd=sotd, challenge=challenge, session=db_session, allowed_hashtag_dict={}
        )
        db_session.flush()

        for key, val in expected_output.items():
            assert val == [md.item for md in parsed[key]].__repr__()

        assert set(expected_output.keys()) == set(parsed.keys())


def test_meta_info(app_with_keyword_challenges):
    db_session = get_db()
    with db_session.begin():
        author = db_session.get(orms.User, 1)
        post = db_session.get(orms.Post, 1)
        challenge = db_session.query(orms.ChallengeBase)[:1][0]
        sotd = orms.SOTD(
            author=author,
            post=post,
            created_utc=3,
            retrieved_on=4,
            text="""
* **Brush:** My Hands #STANKY #STAKNY
* **Razor:** Murker 37x #PLASTIC""",
            permalink="https://lin.k",
        )

        challenge.sotds.append(sotd)
        db_session.flush()
        parsed = orms.WSDBBrush.parse_all(
            sotd=sotd, challenge=challenge, session=db_session, allowed_hashtag_dict={}
        )

        assert parsed["razors"][0].__repr__() == f"MetaInfo(Razor(Murker 37x), {sotd})"
