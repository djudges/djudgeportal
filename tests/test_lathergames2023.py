#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   test_lathergames2023.py

@author Dj Djundjila <djundjila.gitlab@cloudmail.altermail.ch>

@date   16 Apr 2023

@brief  tests for the LatherGames 2023 class

Copyright © 2023 Dj Djundjila, TTS Rebuild Committee

DjudgePortal is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

DjudgePortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from flask import g
from DjudgePortal import orms
from DjudgePortal.db import get_db

from collections import namedtuple


def test_item_parser(lg_challenge_with_sotd):
    db_session = get_db()
    with db_session.begin():
        sotd = db_session.get(orms.SOTD, g.sotd_pk)
        razor_assoc, brush_assoc = sotd.meta_info
        assert razor_assoc.item.name == "El Razoro"
        assert razor_assoc.sotd == sotd
        assert razor_assoc.hashtags[0].tag == "#CHROME"
        assert sotd.meta_info[0].item == razor_assoc.item
        assert sotd.meta_info[0].sotd == sotd
        assert sotd.meta_info[1].item == brush_assoc.item
        assert sotd.meta_info[1].sotd == sotd
        assert len(sotd.meta_info) == 2


def test_djudgement_tally(lg_challenge_with_djudgement):
    db_session = get_db()
    with db_session.begin():
        sotd = db_session.get(orms.SOTD, g.sotd_pk)
        lg = db_session.get(orms.ChallengeBase, g.lg_pk)
        lg.update_tally_for_user(sotd.author, db_session)

        for tally in sotd.author.sotd_points_tallies:
            # TODO: reinstate this when meta-tallies or group tallies exist
            # if tally.name == "djudge_points":
            #     assert tally.value == 2.0 / 3.0
            # assert tally.challenge == lg
            pass


djudge_eval = namedtuple(
    "DjudgeEvaluation",
    "disqualification, above_and_beyond, topical_consistency, enjoyable, offensive",
)

obj_pts = namedtuple(
    "ObjectivePoints",
    "theme_points, relevant_postfrag, daily_challenge, special_challenge",
)


@pytest.mark.parametrize(
    ("obj_pts, djudge_eval, score"),
    [
        (
            obj_pts(
                theme_points=True,
                relevant_postfrag=True,
                daily_challenge=True,
                special_challenge=True,
            ),
            djudge_eval(
                disqualification=None,
                above_and_beyond=True,
                topical_consistency=True,
                enjoyable=True,
                offensive=True,
            ),
            {
                "djudge_points": 2 * 2.0 / 9.0,
                "Enjoyable": 2.0 / 9.0,
                "Above and beyond": 2.0 / 9.0,
                "Topical consistency": 2.0 / 9.0,
                "Offensive": -2.0 / 9.0,
                "Daily Theme Points": 2.0,
                "Relevant Post Shave & Fragrance": 0.2,
                "Daily Challenge": 0.2,
                "Special Challenge (1/2)": 0.5,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification=None,
                above_and_beyond=True,
                topical_consistency=True,
                enjoyable=True,
                offensive=False,
            ),
            {
                "djudge_points": 3 * 2.0 / 9.0,
                "Enjoyable": 2.0 / 9.0,
                "Above and beyond": 2.0 / 9.0,
                "Topical consistency": 2.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification=None,
                above_and_beyond=True,
                topical_consistency=True,
                enjoyable=False,
                offensive=False,
            ),
            {
                "djudge_points": 2 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 2.0 / 9.0,
                "Topical consistency": 2.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification=None,
                above_and_beyond=True,
                topical_consistency=False,
                enjoyable=False,
                offensive=False,
            ),
            {
                "djudge_points": 2 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 2.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification=False,
                above_and_beyond=False,
                topical_consistency=False,
                enjoyable=False,
                offensive=False,
            ),
            {
                "djudge_points": 0.0 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 0.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification=None,
                above_and_beyond=False,
                topical_consistency=False,
                enjoyable=False,
                offensive=True,
            ),
            {
                "djudge_points": -2.0 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 0.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": -2.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification="DQ for reasons",
                above_and_beyond=False,
                topical_consistency=False,
                enjoyable=False,
                offensive=False,
            ),
            {
                "djudge_points": 0.0 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 0.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification="DQ for reasons",
                above_and_beyond=True,
                topical_consistency=False,
                enjoyable=False,
                offensive=False,
            ),
            {
                "djudge_points": 0.0 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 0.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
        (
            obj_pts(
                theme_points=False,
                relevant_postfrag=False,
                daily_challenge=False,
                special_challenge=False,
            ),
            djudge_eval(
                disqualification="DQ for reasons",
                above_and_beyond=False,
                topical_consistency=False,
                enjoyable=False,
                offensive=True,
            ),
            {
                "djudge_points": 0.0 * 2.0 / 9.0,
                "Enjoyable": 0.0 / 9.0,
                "Above and beyond": 0.0 / 9.0,
                "Topical consistency": 0.0 / 9.0,
                "Offensive": 0.0 / 9.0,
                "Daily Theme Points": 0.0,
                "Relevant Post Shave & Fragrance": 0.0,
                "Daily Challenge": 0.0,
                "Special Challenge (1/2)": 0.0,
            },
        ),
    ],
)
def test_djudgement_value(
    lg_challenge_with_2_sotd_2_lg_legacy,
    obj_pts,
    djudge_eval,
    score,
):
    db_session = get_db()
    with db_session.begin():
        # invite djudgement
        djudge = db_session.get(orms.User, g.djudge1_pk)
        for i in range(2):
            lg = db_session.get(orms.ChallengeBase, g.lg_pks[i])
            for j in range(2):
                sotd = db_session.get(orms.SOTD, g.sotd_pks[i][j])
                if j == 0:
                    template = lg.create_invite_regular_challenge(db_session=db_session)
                elif j == 1:
                    template = lg.create_invite_special_challenge(db_session=db_session)
                campaign = orms.DjudgementCampaign(
                    template=template,
                    instructions=(
                        "Today's theme is topical and the challenge is " "challenging"
                    ),
                )
                db_session.add(campaign)
                lg.sotds.append(sotd)
                db_session.flush()
                invitation = campaign.invite_sotd(djudge=djudge, sotd=sotd)
                db_session.add_all((invitation, template))
                db_session.flush()
                db_session.refresh(lg)
                assert invitation.state == orms.States.Unacknowledged
                assert invitation.pk is not None

                # djudge sotd
                djudgement = orms.Djudgement(djudgement_invitation=invitation)
                db_session.add(djudgement)
                djudgement.scores.append(
                    lg.categories["Disqualification"].new_score(
                        value=djudge_eval.disqualification, djudgement=djudgement
                    )
                )
                djudgement.scores.append(
                    lg.categories["Above and beyond"].new_score(
                        value=djudge_eval.above_and_beyond, djudgement=djudgement
                    )
                )

                djudgement.scores.append(
                    lg.categories["Topical consistency"].new_score(
                        value=djudge_eval.topical_consistency, djudgement=djudgement
                    )
                )

                djudgement.scores.append(
                    lg.categories["Enjoyable"].new_score(
                        value=djudge_eval.enjoyable, djudgement=djudgement
                    )
                )

                djudgement.scores.append(
                    lg.categories["Offensive"].new_score(
                        value=djudge_eval.offensive, djudgement=djudgement
                    )
                )

                djudgement.scores.append(
                    lg.categories["Daily Theme Points"].new_score(
                        value=obj_pts.theme_points, djudgement=djudgement
                    )
                )

                djudgement.scores.append(
                    lg.categories["Relevant Post Shave & Fragrance"].new_score(
                        value=obj_pts.relevant_postfrag, djudgement=djudgement
                    )
                )

                if j == 0:
                    djudgement.scores.append(
                        lg.categories["Daily Challenge"].new_score(
                            value=obj_pts.daily_challenge, djudgement=djudgement
                        )
                    )
                elif j == 1:
                    assert "Special Challenge (1/2)" in lg.categories.keys()

                    djudgement.scores.append(
                        lg.categories["Special Challenge (1/2)"].new_score(
                            value=obj_pts.special_challenge, djudgement=djudgement
                        )
                    )

        lg.update_tally_for_user(sotd.author, db_session)

        assert sotd.author.tags == [
            db_session.query(orms.WSDBHashTag).filter_by(tag="#CHROME").one()
        ]
        tallies = sotd.author.sotd_points_tallies
        for tally in tallies:
            scores = tally.scores
            if "Challenge" in tally.name:
                assert len(scores) == 1
                assert tally.name in score.keys()
                assert tally.value == score[tally.name]
                assert tally.__repr__() == f"Tally({lg.name}::{tally.category.title})"
            else:
                assert len(scores) == 2
                assert tally.value == 2 * score[tally.name]
