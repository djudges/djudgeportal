# DjudgePortal

Is a Python library to help run and djudge wetshaving contests. Djudges can score SOTD posts assigned to them and organisers can follow scores, rankings, and custom analyses.

For production, DjudgePortal relies on [flask](https://flask.palletsprojects.com/en/2.2.x/) as a web framework, [sqlalchemy](https://www.sqlalchemy.org/) for managing SQL access and Object Relational Mapping, and [praw](https://praw.readthedocs.io/en/stable/) for reddit access..

For development, testing and continuous integration, DjudgePortal uses [pytest](https://docs.pytest.org/en/7.3.x/) for testing with [unittest.Mock](https://docs.python.org/3/library/unittest.mock.html) to mock reddit web accesses where necessary.

It requires a PostgreSQL server for both testing and production.


## Getting started as a dev

### Install dependencies

(It is recommended, but not necessary, to install all the Python dependencies in a virtual environment)

```
$ pip install -r requirements.txt
```
installs all the requirements to run DjudgePortal, and 

```
$ pip install black
$ pip install pyflakes
```
installs all the requirements for linting and static analysis for common bugs.

Note that you need to have the dev headers and libraries for postgresql installed in your system for the first step to work.

### Setup PostgreSQL

You will need two databases to run CI tests and a development server to click around.
The following uses the placeholders `<test_user>`, `<password>`, `<dev_db>`, and `<ci_db>` for the user name, password, development server database and CI tests database.

Log into your postgres server as the administrator (usually user `postgres`)

```
> $ sudo su - postgres #on a debian based system, may be different for your os
> # psql
> # create user <test_user> with encrypted password '<password>';
> # create database <dev_db> encoding UTF8;
> # create database <ci_db> encoding UTF8;
> # grant all privileges on database <dev_db>  to <test_user> ;
> # grant all privileges on database <ci_db>  to <test_user> ;
> # exit
```

### Make a dev install

This step assures that the test suites can run against an installed target, rather than the sources, which can get messy quick

```
$ ./setup.py develop
```

### Test Environment

Two `.env` files define how DjudgePortal behaves during `pytest` and on the local development server. Example skeletons for these are;

`.env`:

```
SECRET_KEY='dev'
SQLALCHEMY_DATABASE_URI='postgresql://<test_user>:<password>@localhost/<dev_db>?client_encoding=utf8'
FLASK_DEBUG=True
DEBUG=True
ROOT_ADMIN='<test_user>'
REDDIT_CONFIG='REDDIT_BOT_NAME'
REDDIT_IDENTITY_BOT='REDDIT_BOT_NAME'
ACCESS_CODE_SIZE=1000000
ACCESS_CODE_TTL=600
```
You can leave the `REDDIT_BOT_NAME` at a dummy name if you only use CI, else you will need to set up app access to reddit, see the [praw](https://praw.readthedocs.io/en/stable/) doc for details.

`.env_test`:

```
SECRET_KEY='dev'
SQLALCHEMY_DATABASE_URI='postgresql://<test_user>:<password>@localhost/<ci_db>?client_encoding=utf8'
FLASK_DEBUG=True
ENV='development'
DEBUG=True
ROOT_ADMIN='admin'
REDDIT_CONFIG='REDDIT_BOT_NAME'
REDDIT_IDENTITY_BOT='REDDIT_BOT_NAME'
ACCESS_CODE_SIZE=1000000
ACCESS_CODE_TTL=600
WTF_CSRF_ENABLED = False
```

### Running test suites:

You can run the test suit and collect coverage data using 
```
$ coverage run -m pytest
============================= test session starts ==============================
platform linux -- Python 3.9.2, pytest-7.2.2, pluggy-1.0.0
rootdir: /home/junge/Programme/personal/djudgeportal
collected 17 items                                                             

tests/test_auth.py ......                                                [ 35%]
tests/test_challenges.py .                                               [ 41%]
tests/test_db.py ..                                                      [ 52%]
tests/test_factory.py ..                                                 [ 64%]
tests/test_sotds.py ...                                                  [ 82%]
tests/test_user.py ...                                                   [100%]

============================= 17 passed in 16.26s ==============================
$ 
```

A quick coverage report can then be generated using

```
$ coverage report
Name                                     Stmts   Miss  Cover
------------------------------------------------------------
.env_test                                   11      0   100%
DjudgePortal/__init__.py                    48      0   100%
DjudgePortal/auth.py                        95     38    60%
DjudgePortal/base.py                         3      0   100%
DjudgePortal/blueprints/__init__.py          1      0   100%
DjudgePortal/blueprints/analyses.py         63     46    27%
DjudgePortal/blueprints/challenges.py      210    143    32%
DjudgePortal/blueprints/djudgements.py     104     82    21%
DjudgePortal/blueprints/posts.py            22     15    32%
DjudgePortal/blueprints/sotds.py            28      8    71%
DjudgePortal/blueprints/users.py            27     16    41%
DjudgePortal/db.py                         289    197    32%
DjudgePortal/orms/__init__.py                8      0   100%
DjudgePortal/orms/analysis.py               42     23    45%
DjudgePortal/orms/challenge.py              78     10    87%
DjudgePortal/orms/djudgement.py             27      0   100%
DjudgePortal/orms/score.py                  78      7    91%
DjudgePortal/orms/sotd.py                   52      0   100%
DjudgePortal/orms/user.py                   69     17    75%
tests/__init__.py                            0      0   100%
tests/conftest.py                           89      1    99%
tests/test_auth.py                          49      0   100%
tests/test_challenges.py                    20      0   100%
tests/test_db.py                            15      0   100%
tests/test_factory.py                        8      0   100%
tests/test_sotds.py                         39      0   100%
tests/test_user.py                          24      0   100%
------------------------------------------------------------
TOTAL                                     1499    603    60%
```

And to get really useful coverage information, you can generate annotated HTML:

```
$ coverage html
Wrote HTML report to htmlcov/index.html
```
which then looks like this:

![Screen shot of rendered coverage HTML](static/coverage.png)

